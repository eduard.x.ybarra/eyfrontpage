import React from 'react';
import { useNavigate } from "react-router-dom";
import { Link } from "react-scroll";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faEye } from '@fortawesome/free-solid-svg-icons'
import firstportfolio from './videos/Firstportfolio.mp4';
import secondportfolio from './videos/Secondportfolio.mp4';
import budatchdesign from './videos/Bdcompany.mp4';
import frontbg from './videos/fadedsun.jpg';
import './FrontPage.css';

const FrontPage = () => {
  let navigate = useNavigate();

  return (
    <div className="front-page">
      <div className={"project-front-title"} id={"project1"} style={{backgroundImage: `url(${frontbg})`}}>
        <p className={"front-title"}>EDUARD YBARRA</p>
      </div>
      <div className={"front-video-container"}>
        <div className={"project-front"}>
          <div className={"video-capsulator"}  style={{height: "75vh"}}>
            <video className={"front-video"} src={budatchdesign} autoPlay muted loop></video>
            <div className={"video-overlay"}>
              <h2>Check This <span>Out!</span></h2>
              <p>this is some text.</p>
            </div>
          </div>
          <div className={"video-informant"}>
            <div className={"project-text-link"} onClick={() => navigate("/budatchdesign")}>
              <FontAwesomeIcon icon={faEye} />
              <p>View Project</p>
            </div>
            <Link activeClass="active-section" to="project3" spy={true} smooth={true} duration={500}>
              <div className={"project-indicator-up"}>
                <i className={"project-arrow-up"}></i>
                <div className={"project-line-up"}></div>
              </div>
            </Link>
            <Link activeClass="active-section" to="project2" spy={true} smooth={true} duration={500}>
              <div className={"project-indicator-down"}> 
                <div className={"project-line-down"}></div>
                <i className={"project-arrow-down"}></i>
              </div>
            </Link>
          </div>
        </div>
        <div className={"project-front"}>
          <div className={"video-capsulator"} id={"project2"} style={{height: "100vh"}}>
            <video className={"front-video"} src={secondportfolio} autoPlay muted loop></video>
            <div className={"video-overlay"}>
              <h2>Check This <span>Out!</span></h2>
              <p>this is some text.</p>
            </div>
          </div>
          <div className={"video-informant"}>
            <div className={"project-text-link"} onClick={() => navigate("/secondportfolio")}>
              <FontAwesomeIcon icon={faEye} />
              <p>View Project</p>
            </div>
            <Link activeClass="active-section" to="project1" spy={true} smooth={true} duration={500}>
              <div className={"project-indicator-up"}>
                <i className={"project-arrow-up"}></i>
                <div className={"project-line-up"}></div>
              </div>
            </Link>
            <Link activeClass="active-section" to="project3" spy={true} smooth={true} duration={500}>
              <div className={"project-indicator-down"}> 
                <div className={"project-line-down"}></div>
                <i className={"project-arrow-down"}></i>
              </div>
            </Link>
          </div>
        </div>
        <div className={"project-front"}>
          <div className={"video-capsulator"} id={"project3"} style={{height: "100vh"}}>
            <video className={"front-video"} src={firstportfolio} autoPlay muted loop></video>
            <div className={"video-overlay"}>
              <h2>Check This <span>Out!</span></h2>
              <p>this is some text.</p>
            </div>
          </div>
          <div className={"video-informant"}>
            <div className={"project-text-link"} onClick={() => navigate("/firstportfolio")}>
              <FontAwesomeIcon icon={faEye} />
              <p>View Project</p>
            </div>
            <Link activeClass="active-section" to="project2" spy={true} smooth={true} duration={500}>
              <div className={"project-indicator-up"}>
                <i className={"project-arrow-up"}></i>
                <div className={"project-line-up"}></div>
              </div>
            </Link>
            <Link activeClass="active-section" to="project1" spy={true} smooth={true} duration={500}>
              <div className={"project-indicator-down "}> 
                <div className={"project-line-down"}></div>
                <i className={"project-arrow-down"}></i>
              </div>
            </Link>
          </div>
        </div>
      </div>
    </div>
  )
}

export default FrontPage;
