import React from 'react';

import SeperatorLine from './SeperatorLine.jsx';

const HeaderVideoAssister = () => {
  return (
    <React.Fragment>
      <div style={{height: "20px", backgroundColor: "white", marginBottom: "-20px"}} />
      <div style={{height: "20px", backgroundColor: "white"}}>
        <SeperatorLine moveRight={true} />
      </div>
      <div style={{height: "20px", backgroundColor: "white"}} />
    </React.Fragment>
  )
}

export default HeaderVideoAssister;