import React, { useState, useEffect } from 'react';
import MovieInformation from './MovieInformation.jsx';
import '../css/CreditsByYear.css';
import nullposter from '../images/nullposter.png';

const imagePath = "https://image.tmdb.org/t/p/w200";

const CreditsByYear = ({ year, credits }) => {
  const[showYearCredits, setShowYearCredits] = useState(true)
  const[yearCredits, setYearCredts] = useState([]);

  useEffect(() => {
    const sameYearCredits = [];

    for(const credit of credits) {
      if(credit.hasOwnProperty('release_date')) {
        if(credit.release_date.slice(0, 4) === year) {
          sameYearCredits.push(credit)
        }
      }
      if(credit.hasOwnProperty('first_air_date')) {
        if(credit.first_air_date.slice(0, 4) === year) {
          sameYearCredits.push(credit)
        }
      }
    }
    
    setYearCredts(sameYearCredits);
  }, [year, credits])

  const handleOnClick = () => {
    setShowYearCredits(!showYearCredits);
  }
  
  return (
    <div>
      <h5 className={'text-center'} onClick={() => handleOnClick()}>{year !== ""  ? (year) : ("Upcoming")}</h5>
      {showYearCredits ? (
        <div className={'read-more-bio-content'}>
          <i className="fa fa-angle-up fa-sm" style={{marginTop: "-0.75em", cursor: "pointer"}} onClick={() => handleOnClick()}></i>
        </div>
      ) : (
        <div className={'read-more-bio-content'}>
          <i className="fa fa-angle-down fa-sm" style={{marginTop: "-0.75em", cursor: "pointer"}} onClick={() => handleOnClick()}></i>
        </div>
			)}
      {showYearCredits ? (
        <ul className={"list-group"}>
        {yearCredits.map((credit) => (
          <li key={credit.id} className={"list-group-item movies-list-item"} style={{height: "8em"}}>
            <MovieInformation poster={credit.poster_path !== null ? (imagePath + credit.poster_path) : (nullposter)} title={credit.title} releaseDate={credit.release_date} character={credit.character} firstAirDate={credit.first_air_date} job={credit.job} name={credit.name} overview={credit.overview}/>
          </li>
        ))}
        </ul>
      ) : (null)}
    </div>
  )
}

export default CreditsByYear;