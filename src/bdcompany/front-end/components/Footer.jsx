import React, { useState, useEffect } from 'react';
import { Link, useLocation } from "react-router-dom";
import EmailBox from './EmailBox.jsx';
import logocomplete from '../images/BDcomplete.svg';
import logo from '../images/BDlogo.svg';
import '../css/Footer.css';

const Footer = () => {
  const [showBox, setShowBox] = useState(false);
  const [currentLink, setCurrentLink] = useState("");

  function usePageViews() {
    let location = useLocation();

    useEffect(() => {
      setCurrentLink(location.pathname);
    }, [location]);
  }

  usePageViews();
  
  const handlePopUp = () => {
    setShowBox(!showBox);
  }

  return (
    <div className={"footer-section"} >
      <div className={"footer-details"}>
        <div className={"footer-bottom-links"}>
          <Link className={"bottom-link"} to="/budatchdesign">
            <p className={currentLink === ("/budatchdesign") ? ("bottom-link-selected") : ("bottom-link-unselected")}>Home</p>
          </Link>
          <Link className={"bottom-link"} to="/budatchdesign/about">
            <p className={currentLink === ("/budatchdesign/about") ? ("bottom-link-selected") : ("bottom-link-unselected")}>About</p>
          </Link>
          <Link className={"bottom-link"} to="/budatchdesign/style">
            <p className={currentLink === ("/budatchdesign/style") ? ("bottom-link-selected") : ("bottom-link-unselected")}>Style</p>
          </Link>
          <Link className={"bottom-link"} to="/budatchdesign/latest">
            <p className={currentLink === ("/budatchdesign/latest") ? ("bottom-link-selected") : ("bottom-link-unselected")}>Latest</p>
          </Link>
          <Link className={"bottom-link"} to="/budatchdesign/contact">
            <p className={currentLink === ("/budatchdesign/contact") ? ("bottom-link-selected") : ("bottom-link-unselected")}>Contact</p>
          </Link>
          <Link className={"bottom-link"} to="/budatchdesign/budatchprojects">
            <p className={currentLink === ("/budatchdesign/budatchprojects") ? ("bottom-link-selected") : ("bottom-link-unselected")}>Projects</p>
          </Link>
        </div>
        <img className={"footer-image"} src={logocomplete} alt="Budatch Design"></img>
        <div className={"footer-email"} onClick={handlePopUp}>
          <span>Email Budatch Design</span>
          <i className="fa fa-envelope fa-2x"></i>
        </div>
        {showBox ? <EmailBox handlePopUp={handlePopUp} /> : null}
      </div>
    </div>
  )
}

export default Footer;