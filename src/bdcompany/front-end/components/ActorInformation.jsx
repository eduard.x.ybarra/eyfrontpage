import React, { useState, useEffect, useRef } from 'react';

import ActorImage from './ActorImage.jsx';
import ActorAward from './ActorAward.jsx';
import Trivia from './Trivia.jsx';

import '../css/ActorInformation.css';

function useInterval(callback, delay) {
  const savedCallback = useRef();

  useEffect(() => {
    savedCallback.current = callback;
  }, [callback]);

  useEffect(() => {
    function tick() {
      savedCallback.current();
    }
    if (delay !== null) {
      let id = setInterval(tick, delay);
      return () => clearInterval(id);
    }
  }, [delay]);
}

const ActorInformation = ({ externalIds, name, profileImages, department, birthday, birthplace, awards, trivias, knownFor, knownForImages, jobs, titles, posters}) => {
  const [imageIndex, setImageIndex] = useState(0);
  const [triviaIndex, setTriviaIndex] = useState(0);
  const [image, setImage] = useState(profileImages[imageIndex]);
  const [trivia, setTrivia] = useState(trivias[triviaIndex]);
  const [swapTriviaActivated, setSwapTriviaActivated] = useState(true);
  const [swapProfileImageActivated, setSwapProfileImageActivated] = useState(true);

  useEffect(() => {
    setTrivia(trivias[0]);
  }, [trivias])

  const _handleOnClickDot = (index) => {
    setImage(profileImages[index]);
    setImageIndex(index);
    setSwapProfileImageActivated(false);
  }

  const _handleClickArrow = (index) => {
    let currentIndex = index;

    if(index < 0) {
      currentIndex = profileImages.length - 1;
    }
    if(index >= profileImages.length) {
      currentIndex = 0;
    }
    
    setImage(profileImages[currentIndex]);
    setImageIndex(currentIndex);
    setSwapProfileImageActivated(false);
  }

  const _handleOnClickTriva = (index) => {
    let newTriviaIndex = index;

    if(newTriviaIndex > trivias.length - 1) {
      newTriviaIndex = 0;
    }

    if(newTriviaIndex < 0) {
      newTriviaIndex = trivias.length - 1;
    }

    setTrivia(trivias[newTriviaIndex]);
    setTriviaIndex(newTriviaIndex);
    setSwapTriviaActivated(false);
  }

  const swapProfileImage = () => {
    let profileImageId = imageIndex;

    if(profileImages.length > profileImageId) {
      profileImageId = profileImageId + 1;
    }

    if(profileImages.length === profileImageId) {
      profileImageId = 0;
    }
    
    setImage(profileImages[profileImageId]);
    setImageIndex(profileImageId);
  }

  const swapTrivia = () => {
    let currentTrivia = triviaIndex;

    if(trivias.length > currentTrivia) {
      currentTrivia = currentTrivia + 1;
    }

    if(trivias.length === currentTrivia) {
      currentTrivia = 0;
    }
    
    setTrivia(trivias[currentTrivia]);
    setTriviaIndex(currentTrivia);
  }

  useInterval(() => {
    swapProfileImage();
  }, swapProfileImageActivated ? 4000 : null);

  useInterval(() => {
    swapTrivia();
  }, swapTriviaActivated ? 2000 : null);

  return (
    <div className={'top-actor-container row justify-content-around'}>
      <ActorImage image={image} imageIndex={imageIndex} profileImages={profileImages} externalIds={externalIds} handleOnClickDot={_handleOnClickDot} handleClickArrow={_handleClickArrow}/>
      <div className={'resume-container'}>
        <div className={'actor-mini-bio row justify-content-between'}>
          <div className={'actor-text'}>
            <div className={'actor-titles'}>
              <h3 style={{fontSize: "30px", marginBottom: "1px"}}>{name}</h3>
              <div className={'actor-jobs row'}>
              {jobs ? (
                jobs.map((job, index) => (
                  <div key={index} style={{display: "flex"}}>
                    <p>{job}</p>
                    <h6>{jobs.length - 1 !== index ? ("|") : (null)}</h6>
                  </div>
                ))
              ) : (null)}
              </div>
            </div>
            <p>Born {birthday} in {birthplace}</p>
          </div>
          <ActorAward awards={awards}/>
        </div>
        <Trivia trivia={trivia} triviaIndex={triviaIndex} trivias={trivias} handleOnClickTriva={_handleOnClickTriva}/>
        <p style={{fontSize: "20px"}}>Known for {department}</p>
        <div>
        {knownForImages.length === 0 ? (
          <div className={'known-for-poster'}>
            {posters.map((image, index) => (
              <img key={index} src={image} alt={image}/>
            ))}
          </div>
        ) : (
          <div className={'known-for-poster'}>
            {knownForImages.map((image, index) => (
              <img key={index} src={image} alt={image}/>
            ))}
          </div>
        )}
        {knownFor.length === 0 ? (
          <div className={'known-for-title'}>
            {titles.map((title, index) => (
              <p key={index}>{title}</p>
            ))}
          </div>
        ) : (
          <div className={'known-for-title'}>
            {knownFor.map((title, index) => (
              <p key={index}>{title}</p>
            ))}
          </div>
        )}
        </div>
      </div>
    </div>
  )
}

export default ActorInformation;
