import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import '../css/Latest.css';

const LatestImageBox = ({ picture, name, pictureWidth }) => {
  const[gridSize, setGridSize] = useState("");

  useEffect(() => {
    setGridSize('col-' + pictureWidth)
  }, [pictureWidth])

  return (
    <div className={gridSize + ` article`}>
      <div className={"article-container"}>
        <Link to={`/budatchdesign/latest/${name}`}>
          <img src={require('../images/' + picture)} alt={name} className={`article-image`} />
          <div className={"article-bottom-text"}>{name}</div>
        </Link>
      </div>
    </div>
  )
}

export default LatestImageBox;