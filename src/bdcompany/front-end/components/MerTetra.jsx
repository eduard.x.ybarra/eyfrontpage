import React from 'react'
import rod from '../cocacola-images/mer/mer-tetra-rod.jpeg';
import gron from '../cocacola-images/mer/mer-tetra-gron.jpeg';
import bla from '../cocacola-images/mer/mer-tetra-bla.jpeg';
import orange from '../cocacola-images/mer/mer-tetra-orange.jpeg';
import tccc from '../cocacola-images/tccc.png';
import '../css/MerTetra.css';


const MerTetra = () => {

  const inViewport = (entries, observer) => {
    entries.forEach(entry => {
      entry.target.classList.toggle("is-inViewport", entry.isIntersecting);
    });
  };
  
  const Obs = new IntersectionObserver(inViewport);
  const obsOptions = {};
  
  const elements_inViewport = document.querySelectorAll('[tetra-inviewport]');
    elements_inViewport.forEach(element => {
      Obs.observe(element, obsOptions);
  });

  return (
    <div className={'tetra-container'} style={{overflowX: "hidden"}} >
      <div className={'tetra-mer-information'}>
        <p>
        Finns även i 200 ml <strong>Tetra Pak®</strong>
        </p>
      </div>
      <div tetra-inviewport="slide-in" className={'tetra-images'}>
        <img src={rod} alt={'Tetra Pak'}/>
        <img src={gron} alt={'Tetra Pak'}/>
        <img src={bla} alt={'Tetra Pak'}/>
        <img src={orange} alt={'Tetra Pak'}/>
      </div>
      <div className={'tetra-mer-information'}>
        <p>
        Denna förpackning är <strong>100%</strong> återvinningsbar så glöm inte att återvinna när du druckit klart
        </p>
      </div>
      <div style={{borderBottom: "1px solid black"}}/>
      <div className={'mer-information'}>
        <p>
        MER är en dryck utan kolsyra som lanserades i Sverige 1962 och snabbt blev en favorit hos folket. En klassiker med en kompromisslös passion för fruktiga smaker. En svensk fruktdryck med en unik smak - det är inte riktigt en juice, inte saft eller läsk - den är MER. MER är aldrig kolsyrad och kompromissar inte när det kommer till smakupplevelsen.
        </p>
      </div>
      <div className={'additional-mer-information'}>
        <p>
        Sedan 1997 ägs varumärket av <img src={tccc} alt={'The Coca-Cola Company'}/>
        </p>
      </div>
    </div>
  )
}

export default MerTetra;