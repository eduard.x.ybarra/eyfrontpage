import React from 'react';
import HeaderVideo from './HeaderVideo.jsx';
import StyleBottom from './StyleBottom.jsx';
import StyleTop from './StyleTop.jsx';
import '../css/Style.css';

import style from '../images/Style.mp4';

const Style = ({ id, styleData, swapCompany }) => {

  return (
    <div>
      <HeaderVideo video={style} path={"Style"} />
      <StyleTop id={id} styleData={styleData} />
      <div className={"d-flex"}>
        <div className={"separator-line-reversed"}/>
        <div className={"separator-line"}/>
      </div>
      <div className={"text-center"}>
        <h2>More Budatch Design</h2>
      </div>
      <StyleBottom id={id} styleData={styleData} swapCompany={swapCompany} />
    </div>
  )
}

export default Style;