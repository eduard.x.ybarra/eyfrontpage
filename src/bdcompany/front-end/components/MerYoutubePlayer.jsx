import React from 'react';
import ReactPlayer from 'react-player'

import '../css/MerYoutubePlayer.css';

import MER1 from '../cocacola-images/mer/MER1.mp4';
import MER2 from '../cocacola-images/mer/MER2.mp4';
import MER3 from '../cocacola-images/mer/MER3.mp4';
import MER4 from '../cocacola-images/mer/MER4.mp4';

const MerYoutubePlayer = () => {

  return (
    <div className={'mer-youtube-section'}>
      <div className={'mer-youtube-container'}>
        <div className={'side-clip-mer'}>
          <video className={"img-fluid"} src={MER1} type="video/mp4" muted={true} autoPlay loop/>
          <video className={"img-fluid"} src={MER2} type="video/mp4" muted={true} autoPlay loop/>
        </div>
        <div className={'youtube-wrapper'}>
          <ReactPlayer className={'youtube-clip'} url={'https://youtu.be/ZAVZymSO6AM'} controls={true} width={'100%'} height={'100%'}/>
        </div>
        <div className={'side-clip-mer'}>
          <video className={"img-fluid"} src={MER3} type="video/mp4" muted={true} autoPlay loop/>
          <video className={"img-fluid"} src={MER4} type="video/mp4" muted={true} autoPlay loop/>
        </div>
      </div>
      <div className={'bottom-section row justify-content-center'}>
        <p style={{fontWeight: "600"}}>
          MER är MER än bara ett ord, det betyder något, ett uttryck för MER, MER av vad du älskar, MER kul, MER fruktig smak, för du kan aldrig vara för mycket, det är bara så det är...
        </p>
        <div className={"love-mer"}>
          <div className={'row'} style={{zIndex: "2", position: "relative"}}>
            <p style={{color: "limegreen", fontWeight: "600"}}>”</p>
            <p style={{color: "orange", fontWeight: "600"}}>Älska MER</p>
            <p style={{color: "limegreen", fontWeight: "600"}}>”</p>
          </div>
          <div className="heart-shape"></div>
        </div>
      </div>
    </div>
  )
}

export default MerYoutubePlayer;