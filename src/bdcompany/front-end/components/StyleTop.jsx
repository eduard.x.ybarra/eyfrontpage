import React from 'react';
import '../css/Style.css';

const StyleTop = ({ id, styleData }) => {

  return (
    <div className={"style-top-selected"}>
      <h2>{styleData[id].name}</h2>
      <div className={"style-media-frame"} >
        <video className={"img-fluid style-video"} src={require('../images/' + styleData[id].video)} type="video/mp4" muted={true} autoPlay loop/>
      </div>
      <div className={"style-question-answer"}>
        <h5>{styleData[id].question1}</h5>
        <p>{styleData[id].answer1}</p>
        <h5>{styleData[id].question2}</h5>
        <p>{styleData[id].answer2}</p>
      </div>
      <div className={"d-flex"}>
        <div className={"separator-line"}/>
        <div className={"separator-line-reversed"}/>
      </div>
    </div>
  )
}

export default StyleTop;