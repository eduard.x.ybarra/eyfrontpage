import React, { useState, useEffect } from 'react';
import citrus from '../cocacola-images/mer/mer-citrus.png';
import kiwiapple from '../cocacola-images/mer/mer-kiwiapple.png';
import '../css/MerFruitImage.css';

const transitionStyle = (x) => {
  return {
    transition: "0.4s",
    transform: `translateX(${x})`,
    opacity: "0"
  }
}

const MerFruitImage = ({ juiceId, merNutritionData, juices, juice, juiceFruit, handleJuiceClick }) => {
  const [transition, setTransition] = useState(false);
  const [slideDirection, setSlideDirection] = useState();

  useEffect(() => {
    const timerId = transitionSliderHandler();
    return () => clearTimeout(timerId);
  }, []);
  
  const transitionSliderHandler = () => {
    setTransition(true);

    const timerId = setTimeout(() => {
      setTransition(false);
    }, 450);
    
    return timerId;
  }

  const handleArrowColor = (id) => {
    let newId = id;

    if(id < 0) {
      newId = merNutritionData.length - 1;
    }
    if(id >= merNutritionData.length) {
      newId = 0;
    }

    return merNutritionData[newId].color;
  }

  const handleJuiceSelection = (id) => {
    let newId = id;
    
    if(id < 0) {
      newId = merNutritionData.length - 1;
    }
    if(id >= merNutritionData.length) {
      newId = 0;
    }

    return juices[newId];
  }

  const handleArrowClick = (id, direction) => {
    setSlideDirection(direction)
    handleJuiceClick(id);
    transitionSliderHandler();
  }

  return(
    <div className={'fruit-container row justify-content-center'} style={{height: "340px", overflowX: "hidden"}}>
      <div className={'side-img-container'} style={transition === true ? (transitionStyle(slideDirection)) : (null)}>
        <img className={`${handleJuiceSelection(juiceId - 1) === citrus || handleJuiceSelection(juiceId - 1) === kiwiapple ? ('transparent-small') : ('original-small')} img-fluid`} src={handleJuiceSelection(juiceId - 1)} alt={'Mer dryck'} onClick={() => handleArrowClick(juiceId - 1)}/>
      </div>
      <span className={'fruit-arrow stacker-left fa-stack fa-4x'} onClick={() => handleArrowClick(juiceId - 1, "40%")} style={{cursor: "pointer"}}>
        <i className="fa fa-circle fa-stack-2x icon-background"></i>
        <i className="arrow arrow-left fa fa-angle-left fa-stack-1x"></i>
        <i className="arrow arrow-left fa fa-angle-left fa-stack-1x" style={{color: handleArrowColor(juiceId - 1)}}></i>
        <i className="arrow arrow-left fa fa-angle-left fa-stack-1x" style={{color: handleArrowColor(juiceId - 1)}}></i>
      </span>
      <div className={`img-container`} style={transition === true ? (transitionStyle(slideDirection)) : (null)}>
        <img className={`${juice === citrus || juice === kiwiapple ? ('transparent') : ('original')} img-fluid`} src={juice} alt={'Mer dryck'} />
        <div className={'img-container fruits d-flex justify-content-center'} >
        {Object.keys(juiceFruit).map((fruit, i) => (
          <img key={i} src={juiceFruit[fruit]} alt={'Mer frukter'}/>
        ))}
        </div>
      </div>
      <span className={'fruit-arrow stacker-left fa-stack fa-4x'} onClick={() => handleArrowClick(juiceId + 1, "-40%")} style={{cursor: "pointer"}}>
        <i className="fa fa-circle fa-stack-2x icon-background"></i>
        <i className="arrow arrow-right fa fa-angle-right fa-stack-1x" ></i>
        <i className="arrow arrow-right fa fa-angle-right fa-stack-1x" style={{color: handleArrowColor(juiceId + 1)}}></i>
        <i className="arrow arrow-right fa fa-angle-right fa-stack-1x" style={{color: handleArrowColor(juiceId + 1)}}></i>
      </span>
      <div className={'side-img-container'} style={transition === true ? (transitionStyle(slideDirection)) : (null)}>
        <img className={`${handleJuiceSelection(juiceId + 1) === citrus || handleJuiceSelection(juiceId + 1) === kiwiapple ? ('transparent-small') : ('original-small')} img-fluid`} src={handleJuiceSelection(juiceId + 1)} alt={'Mer dryck'} onClick={() => handleArrowClick(juiceId + 1)}/>
      </div>
    </div>
  )
}

export default MerFruitImage;