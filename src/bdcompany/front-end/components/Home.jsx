import React, { useState, useEffect, useRef } from 'react';
import { useNavigate } from 'react-router-dom';
import LatestHome from './LatestHome.jsx';
import HeaderVideo from './HeaderVideo.jsx';
import QuoteBoxText from './QuoteBoxText.jsx';
import home from '../images/Home.mp4';
import '../css/Home.css';

function useInterval(callback, delay) {
  const savedCallback = useRef();

  useEffect(() => {
    savedCallback.current = callback;
  }, [callback]);

  useEffect(() => {
    function tick() {
      savedCallback.current();
    }
    if (delay !== null) {
      let id = setInterval(tick, delay);
      return () => clearInterval(id);
    }
  }, [delay]);
}

const Home = ({ id, styleData, swapCompany, aboutQuotesData, latestData }) => {
  const[hovered, setHovered] = useState(true);
  const[timer, setTimer] = useState(0);
  const navigate = useNavigate();

  const handleTimer = () => {
    if(hovered) {
      if(timer >= 100){
        handleIncrement();
      } else {
        setTimer(timer + 0.25);
      }
    }
  }

  useInterval(handleTimer, 20);

  const handleMouseEnter = () => {
    setHovered(false);
  }

  const handleMouseLeave = () => {
    setHovered(true);
  }  

  const handleSwapCompany = (id) => {
    swapCompany(id);
    setTimer(0);
  }

  const handleIncrement = () => {
    if(id >= styleData.length - 1) {
      swapCompany(0);
    } else {
      swapCompany(id + 1);
    }
    setTimer(0);
  }

  const handleDecrement = () => {
    if(id <= 0) {
      swapCompany(styleData.length - 1)
    } else {
      swapCompany(id - 1)
    }
    setTimer(0);
  }

  const handleOnClick = () => {
    navigate("/budatchdesign/style");
  }
  
  return (
    <div>
			<HeaderVideo video={home} path={"Home"}/>
      <div className={"home-top-wrapper"}>
        <span className="home-stacker fa-stack fa-4x" onClick={handleDecrement}  style={{cursor: "pointer"}}>
          <i className="fa fa-arrow-left home-arrow"></i>
        </span>
        <div className={"home-media-frame"} >
          <video className={"img-fluid home-video"} src={require('../images/' + styleData[id].video)} type="video/mp4" muted={true} autoPlay loop/>
          <span className={"home-media-text"} onClick={handleOnClick} onMouseEnter={handleMouseEnter} onMouseLeave={handleMouseLeave}>
            <p>Click to view project description</p>
          </span>
        </div>
        <span className="home-stacker fa-stack fa-4x" onClick={handleIncrement} style={{cursor: "pointer"}}>
          <i className="fa fa-arrow-right home-arrow"></i>
        </span>
      </div>
      <div className={"row justify-content-center"}>
      {styleData.map(style => (
        <div key={style.id} className={"chargebar"} onClick={() => handleSwapCompany(style.id)}>
          <div className={`${style.id === id ? ("chargebar chargebar-active") : ({})}`} style={style.id === id ? ({width: `${timer}px`}) : ({width: ""})}></div>
        </div>
      ))}
      </div>
      <div className={"d-flex"}>
        <div className={"separator-line"}/>
        <div className={"separator-line-reversed"}/>
      </div>
			<LatestHome latestData={latestData} style={{height: "250px"}}/>
      <div className={"d-flex"}>
        <div className={"separator-line-reversed"}/>
        <div className={"separator-line"}/>
      </div>
      <QuoteBoxText aboutQuotesData={aboutQuotesData}/>
    </div>
  )
}

export default Home;