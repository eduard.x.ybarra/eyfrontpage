import React, { useState, useEffect } from 'react';
import { useParams, useNavigate } from 'react-router-dom';
import HeaderVideo from './HeaderVideo.jsx';
import ArticlePiece from './ArticlePiece.jsx';
import '../css/Article.css';

import latest from '../images/Latest.mp4';

const Article = ({ latestData }) => {
  const[theId, setTheId] = useState(0);
  const[targetedArticle, setTargetedArticle] = useState({});
  const { name } = useParams();
  const navtigate = useNavigate();

  useEffect(() => {

    const pickedArticle = async () => {
      let targeted = {};
      for(let i = 0; i < latestData.length; i++){
        if(latestData[i].name === name) {
          targeted = latestData[i]
          break;
        }
      }
      setTheId(targeted.id)
      setTargetedArticle(targeted);
    }
    
    pickedArticle();
  }, [latestData, name])
  
  const handleClick = (id, name) => {
    if(id === latestData[id].id){
      setTheId(id);
      setTargetedArticle(latestData[id]);
      //navtigate(`${name}`);
    }
  }

  if(latestData[theId] === undefined) {
    return (
      <div>Loader</div>
    )
  }

  return (
    <div>
      <HeaderVideo video={latest} path={"Latest"}/>
      <div className={"article-section"}>
        <div className={"main-article"}>
          <div className={"main-image"}>
            <img src={require('../images/' + latestData[theId].picture)} alt={targetedArticle.name}/>
          </div>
          <nav className={"article-navbar"}>
            <ul className={"article-items"}>
              {latestData.map(latest => 
                <li className={`item ${latest.id === theId ? ("item-active") : ("")}`} key={latest.id} onClick={() => handleClick(latest.id, latest.name)}>
                  <ArticlePiece latest={latest}/>
                </li>  
              )}
            </ul>
          </nav>
        </div>
      </div>
      <div className={'article-topic'}>
        <h1>{targetedArticle.name}</h1>
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Purus sit amet luctus venenatis lectus magna fringilla urna. Donec adipiscing tristique risus nec feugiat in fermentum posuere. Nec dui nunc mattis enim. Pulvinar proin gravida hendrerit lectus a. Lacus sed viverra tellus in hac habitasse platea. Mi tempus imperdiet nulla malesuada pellentesque elit eget gravida. Amet porttitor eget dolor morbi non arcu risus quis varius. Sed ullamcorper morbi tincidunt ornare. Nec ultrices dui sapien eget mi proin sed libero enim. Magna ac placerat vestibulum lectus mauris.</p>
      </div>
    </div>
  )
}

export default Article;