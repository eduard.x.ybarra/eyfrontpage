import React, { useState } from 'react';
import '../css/ActorImage.css';

const imageURL = "https://image.tmdb.org/t/p/w342/";

function ActorImage ({ image, imageIndex, profileImages, externalIds, handleOnClickDot, handleClickArrow }) {
  const [hovered, setHovered] = useState(false);

  const handleOnClick = (index) => {
    handleOnClickDot(index);
  }
  
  const handleArrowOnClick = (index) => {
    handleClickArrow(index);
  }

  return (
    <div className={'image-container'} onMouseLeave={() => setHovered(false)} onMouseEnter={() => setHovered(true)}>
      <img className={'actor-image'} src={imageURL + image} alt={"actor name..."}/>
      {profileImages.length > 1 ? (
        <div className={'dots row justify-content-center'}>
          {profileImages.map((image, index) => (
            <span key={index} className={`${index === imageIndex ? ('active-dot') : ('dot')}`} style={{cursor: "pointer"}} onClick={() => handleOnClick(index)}/>
          ))}
        </div>
      ) : (null)}
      <div id={'image-left-links'}>
        {externalIds.facebook_id ? (<a href={"https://www.facebook.com/" + externalIds.facebook_id} target="_blank" rel="noopener noreferrer" ><i className="fa fa-facebook fa-2x" style={{color: "#3b5998"}}></i></a>) : (null)}
        {externalIds.facebook_id && externalIds.twitter_id ? (<hr />) : (null)}
        {externalIds.twitter_id ? (<a href={"https://www.twitter.com/" + externalIds.twitter_id} target="_blank" rel="noopener noreferrer" ><i className="fa fa-twitter fa-2x"></i></a>) : (null)}
        {externalIds.instagram_id && (externalIds.twitter_id || externalIds.facebook_id) ? (<hr />) : (null)}
        {externalIds.instagram_id ? (<a href={"https://www.instagram.com/" + externalIds.instagram_id} target="_blank" rel="noopener noreferrer" ><i className="fa fa-instagram fa-2x"></i></a>) : (null)}
      </div>
      {hovered ? (
        <div className={'fade-in'}>
          <span className="stacker-left fa-stack fa-4x" onClick={() => handleArrowOnClick(imageIndex - 1)} style={{cursor: "pointer"}}>
            <i className="fa fa-circle fa-stack-2x icon-background"></i>
            <i className="arrow fa fa-angle-left fa-stack-1x" ></i>
          </span>
          <span className="stacker-right fa-stack fa-4x" onClick={() => handleArrowOnClick(imageIndex + 1)} style={{cursor: "pointer"}}>
            <i className="fa fa-circle fa-stack-2x icon-background"></i>
            <i className="arrow fa fa-angle-right fa-stack-1x" ></i>
          </span>
        </div>
      ) : (null)}
    </div>
  )
}

export default ActorImage;