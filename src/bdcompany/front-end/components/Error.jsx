import React, { useEffect } from 'react';
import logo from '../images/BDlogo.svg';

const Footer = () => {
  
  useEffect(() => {
    window.scrollTo(0, 10)
  }, [])

  return (
    <div style={{backgroundColor: "", height: "900px", justifyContent: "center", display: "flex", alignItems: "center", flexDirection: "column"}}>
      <img src={logo} alt="budatchdesign" style={{width: "30vw"}}/>
      <p style={{fontSize: "40px"}}>Error 404, Page not found!</p>
    </div>
  )
}

export default Footer;