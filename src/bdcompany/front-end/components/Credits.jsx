import React from 'react';

import CreditsByYear from './CreditsByYear.jsx';

const Credits = ({ credits, years }) => {

  return (
    <div>
      <ul className={"list-group"}>
      {years.map((year) => (
        <li key={year} className={"list-group-item"}>
          <CreditsByYear year={year} credits={credits} />
        </li>
      ))}
      </ul>
    </div>
  )
}

export default Credits;
