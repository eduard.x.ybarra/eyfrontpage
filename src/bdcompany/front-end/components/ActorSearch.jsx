import React, { useEffect, useState } from 'react';
import { useNavigate } from "react-router-dom";
import DropdownList from './DropdownList.jsx';
import useAxiosFetchData from './FetchData/useAxiosFetchData.jsx';
import '../css/ActorSearch.css';

const API_KEY = "9ef196d7c4b8f349fad6bb4099995f8d";

const ActorSearch = ({ swapIMDbPerson }) => {
  const [query, setQuery] = useState("Leonard");
  const [{ data, isLoading }, doFetch] = useAxiosFetchData();
  const navigate = useNavigate();

  useEffect(() => {
    console.log(data)
  }, [data])

  const handleInputChange = (event) => {
    setQuery(event.target.value);
  }

  const handleOnChange = (e) => {
    e.preventDefault();
    doFetch(`https://api.themoviedb.org/3/search/person?api_key=${API_KEY}&language=en-US&query=${query}&page=1&include_adult=false&840`);
  }

  const _handlePersonId = (personId) => {
    swapIMDbPerson(personId);
  }

  const handleOnSubmit = (e) => {
    e.preventDefault();
    if(data !== undefined) {
      swapIMDbPerson(data[0].id)
      navigate(`/budatchprojects/${data[0].name}-${data[0].id}`);
    }
  }

  return (
    <div className={"actor-search-wrapper"}>
      <form className={"actor-search"} onSubmit={handleOnSubmit} onChange={(e) => handleOnChange(e)}>
        <input onChange={(event) => handleInputChange(event)} placeholder="Search Actor..."/>
        <i className="fa fa-search fa-2x" type="submit" onClick={handleOnSubmit}></i>
      </form>
      <DropdownList data={data} handlePersonId={_handlePersonId} /> 
    </div>
  )
}

export default ActorSearch;