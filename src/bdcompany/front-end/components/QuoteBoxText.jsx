import React, { useState, useEffect, useRef } from 'react';

import '../css/QuoteBoxText.css';

function useInterval(callback, delay) {
  const savedCallback = useRef();

  useEffect(() => {
    savedCallback.current = callback;
  }, [callback]);

  useEffect(() => {
    function tick() {
      savedCallback.current();
    }
    if (delay !== null) {
      let id = setInterval(tick, delay);
      return () => clearInterval(id);
    }
  }, [delay]);
}

const QuoteBoxText = ({ aboutQuotesData }) => {
  const[randomQuote, setRandomQuote] = useState(aboutQuotesData[0])
  const[backgroundOpacity, setBackgroundOpacity] = useState(false);
  const[triggerQuoteSwap, setTriggerQuoteSwap] = useState(false);
  const[boxWidth, setBoxWidth] = useState(true);
  const[boxOnOff, setBoxOnOff] = useState(false);

  const nextQuote = () => {
    let quoteId = 0;

    if(aboutQuotesData.length > quoteId + 1) {
      quoteId = randomQuote.id + 1;
    }

    if(aboutQuotesData.length === randomQuote.id + 1) {
      quoteId = 0;
    }

    setRandomQuote(aboutQuotesData[quoteId]);
  }

  const resize = () => {
    if(triggerQuoteSwap) {
      nextQuote();
    }
    setTriggerQuoteSwap(!triggerQuoteSwap);
    setBoxWidth(!boxWidth);
    setBackgroundOpacity(!backgroundOpacity)
  }

  useInterval(() => {
    resize();
  }, boxOnOff ? 1500 : null);

  useInterval(() => {
    if(boxWidth === false && boxOnOff === false) {
      setBoxWidth(true)
      setBoxOnOff(false)
      setBackgroundOpacity(false);
    } else {
      setBoxOnOff(!boxOnOff)
    }
  }, 4000);
  
  return (
    <div className={"quote-section-box"}>
      <div className={"row justify-content-center gradient-brackets"} style={{width: boxWidth ? ("72vw") : ("40vw")}}>
        <div className={"gradient-brackets-background"} style={{opacity: backgroundOpacity ? ("1") : ("0")}} />
        <div className={"quote-box"} style={{width: boxWidth ? ("63vw") : ("32vw")}} />
        <div className={"quote-box-assister"} style={{width: boxWidth ? ("36vw") : ("15vw")}}>
          <div className={"quoteation-top"}>
            <p style={{color: "rgb(215, 190, 95)"}}>&lsquo;</p>
            <p style={{color: "rgb(255, 240, 190)"}}>&lsquo;</p>
          </div>
          <div className={"quoteation-bottom"}>
            <p style={{color: "rgb(215, 190, 95)"}}>&rsquo;</p>
            <p style={{color: "rgb(255, 240, 190)"}}>&rsquo;</p>
          </div>
        </div>
        <div className={"quote-text-box"} >
          <p style={{opacity: !boxOnOff ? ("1") : ("0")}}>
          {randomQuote.text}
          </p>
          <strong style={{opacity: !boxOnOff ? ("1") : ("0")}}>
          {randomQuote.quoter}
          </strong>
        </div>
      </div>
      <div>
        {aboutQuotesData.map((quotes, index) => (
          <span key={index} className={`${index === quotes.id ? ('quote-dot') : ('quote-dot-avtive')}`} style={{cursor: "pointer"}}   />
        ))}
      </div>
    </div>
  )
}

export default QuoteBoxText;