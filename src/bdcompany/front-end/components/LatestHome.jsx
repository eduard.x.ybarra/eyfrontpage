import React from 'react';
import { useNavigate } from 'react-router-dom'
import LatestImageBox from './LatestImageBox.jsx';
import '../css/LatestHome.css';

const LatestHome = ({ latestData }) => {
  const navigate = useNavigate();

  const handleClickMore = () => {
    navigate("/budatchdesign/latest");
  }
  
  return (
      <div className={"row justify-content-center"} style={{background: "black", width: "80vw", padding: "3em"}}>
        {latestData.filter(latest => latest.id <= 2).map(latest => (
          <LatestImageBox key={latest.id} picture={latest.picture} name={latest.name} pictureWidth={latest.pictureWidth}/>
        ))}
        <span className={"latest-home-more"} onClick={handleClickMore}>More...</span>
      </div>
  )
}

export default LatestHome;