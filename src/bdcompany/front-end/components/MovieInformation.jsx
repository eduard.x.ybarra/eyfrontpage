import React, { useState, useEffect } from 'react';

import '../css/MovieInformation.css';

const MovieInformation = ({ poster, title, character, job, name, overview }) => {
  const [text, setText] = useState("");
  const [cardName, setCardName] = useState("");
  const [cardTitle, setCardTitle] = useState("");
  const [hovered, setHovered] = useState(false);
  const [stayHovered, setStayHovered] = useState(false);
  const [delayHandler, setDelayHandler] = useState(null);
  const [showCard, setShowCard] = useState(true);

  useEffect(() => {
    if(title && character) {
      setCardTitle(title)
      setCardName(character)
      setText(title + " as " + character);
    }
    if(character === "") {
      if(title) {
        setCardTitle(title)
        setCardName("Self")
        setText(title + " as Self");
      }
    }
    if(name && character) {
      setCardTitle(name)
      setCardName(character)
      setText(name + " as " + character);
    }
    if(character === "") {
      if(name) {
        setCardTitle(name)
        setCardName("Self")
        setText(name + " as Self");
      }
    }
    if(title && job) {
      setCardTitle(title)
      setCardName(job)
      setText(title + " as " + job);
    }
    if(job && name) {
      setCardTitle(name)
      setCardName(job)
      setText(name + " as " + job);
    }
  }, [title, character, job, name])

  const handleOnMouseLeave = () => {
    if(stayHovered === false) {
      setHovered(false)
      setShowCard(true);
    }
    clearTimeout(delayHandler)
  }

  const handleOnMouseEnter = () => {
    if(stayHovered === false){
      setShowCard(false);
      setText("");
      setDelayHandler(setTimeout(() => {
        setHovered(true);
        setText(overview);
      }, 800))
    }
  }

  const handleClick = () => {
    if(text !== "") {
      setStayHovered(!stayHovered)
    }
  }
  
  return (
    <div>
      <div className={'flex-row'} onClick={() => handleClick()} onMouseLeave={() => handleOnMouseLeave()} onMouseEnter={() => handleOnMouseEnter()}>
        <div className={'container'}>
        {hovered ? (
          <div className={'fade-in'} style={{width: `${hovered ? ("95%") : (null)}`, marginLeft: hovered ? ("2em") : (null)}}>
            <p>{text}</p>
          </div>
        ) : (null)}
        <div className={"dummy hovered"}/>
        <div className={"content align-items-center d-flex"} style={{marginLeft: "2em", marginRight: "2em"}}>
          <img src={poster} alt={title}/>
        </div>
        {!hovered ? (
          <div className={'fade-in align-items-center d-flex'} style={{width: `${hovered ? ("95%") : (null)}`}}>
            {showCard ? (
              <div className={'d-flex'}>
                <strong>{cardTitle}</strong><p style={{whiteSpace: "pre-wrap"}}> as </p><p style={{color: "black"}}>{cardName}</p>
              </div>
            ) : (null)}
          </div>
        ) : (null)}
        </div>
      </div>
    </div>
  )
}

export default MovieInformation;