import React from 'react';

const style = {
  height: "4em",
  width: "100%",
  backgroundColor: "black",
  position: "relative"
 }

const HeaderAssister = ({children}) => {
  return (
    <div>
      <div style={style}/>
      {children}
    </div>
  )
}

export default HeaderAssister;