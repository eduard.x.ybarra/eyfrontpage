import { useEffect, useState } from "react";
import axios from "axios";

const API_KEY = "9ef196d7c4b8f349fad6bb4099995f8d";

const useAxiosFetchData = () => {
  const [url, setUrl] = useState(`https://api.themoviedb.org/3/search/person?api_key=${API_KEY}&language=en-US&query=leonardo-dicaprio&page=1&include_adult=false&840`);
  const [data, setData] = useState([]);
  const [isLoading, setIsLoading] = useState(false);

  useEffect(() => {
    const fetchData = async () => {
      setIsLoading(true);

      try {
        const response = await axios(url);
        setData(response.data.results);
      } catch (error) {
        setIsLoading(false); 
      }
      setIsLoading(false); 
    };
    fetchData();
  }, [url]);

  return [{ data, isLoading }, setUrl];
};

export default useAxiosFetchData;