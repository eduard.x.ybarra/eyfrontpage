import { useState, useEffect } from 'react';
import axios from 'axios';

const url = "http://localhost:9000/testAPI";

const useIMDBService = () => {
  const [imdbUrl, setIMDBUrl] = useState("");
  const [awards, setAwards] = useState("");
  const [biographyIMDB, setBiographyIMDB] = useState("");
  const [trivias, setTrivias] = useState([]);
  const [knownFor, setKnownFor] = useState([]);
  const [knownForImages, setKnownForImages] = useState([]);
  const [jobs, setJobs] = useState([]);
  
  useEffect(() => {
    const fetchApi = async () => {
      try {
        const response = await axios.post(url, {imdbUrl});
        const data = response.data;
        setAwards(data.awards);
        setBiographyIMDB(data.biography);
        setTrivias(data.trivias);
        setKnownFor(data.knownFor);
        setKnownForImages(data.knownForImages);
        setJobs(data.jobs);
      } catch (error) {
        console.log(error);
      }
    }
    fetchApi();
  }, [imdbUrl]);

  return [{awards, biographyIMDB, trivias, knownFor, knownForImages, jobs}, setIMDBUrl]
}

export default useIMDBService;