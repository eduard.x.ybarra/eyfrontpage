import React, { useState } from 'react';
import logopowerade from '../cocacola-images/logopowerade.png';
import '../css/PoweradeNutritionSlide.css';

const PoweradeNutritionSlide = ({ nutrition }) => {
  const[swap, setSwap] = useState(true);

  return (
    <div className={'powerade-nutrition-container'}>
      <img src={logopowerade} alt={'Powerade logo'} />
      <h3 style={{color: nutrition.subcolor, marginBottom: "2em"}}>{nutrition.title}</h3>
      <div className={'powerade-ingredients-nutrition'}>
        <strong className={`${swap ? ("selected-option") : (null)}`} onClick={() => setSwap(true)}>Ingredients</strong>
        <strong className={`${swap ? (null) : ("selected-option")}`} onClick={() => setSwap(false)}>Näringsvärde</strong>
      </div>
      {swap ? (
        <div className={'nutrition-container-powerade'}>
          <div className={'nutrition-information-powerade'}>
            <p>{nutrition.description}</p>
          </div>
          <div style={{borderBottom: "solid black 1px", width: "350px", margin: "0 auto"}}/>
          <div className={'nutrition-information-powerade'}>
            <strong>Ingredienser</strong>
            <p>{nutrition.ingredients}</p>
          </div>
        </div>
      ) : (
      <div className={'nutrition-container-powerade'}>
        <div className={'nutrition-line-powerade'}>
          <strong style={{left: "0"}}>
          Näringsvärde
          </strong>
          <strong style={{right: "0"}}>
          per 100 ml
          </strong>
        </div>
        <div className={'nutrition-line-powerade'}>
          <strong style={{left: "0"}}>
          Kalorier
          </strong>
          <strong style={{right: "0"}}>
          {nutrition.energi}
          </strong>
        </div>
        <div className={'nutrition-line-bottomless-powerade'}>
          <p style={{left: "0"}}>
          Fett
          </p>
          <p style={{left: "45%"}}>
          {nutrition.fat}
          </p>
        </div>
        <div className={'nutrition-line-powerade'}>
          <p style={{left: "5%"}}>
          varav mättat fett
          </p>
          <p style={{left: "45%"}}>
          {nutrition.saturatedFat}
          </p>
        </div>
        <div className={'nutrition-line-bottomless-powerade'}>
          <p style={{left: "0"}}>
          Kolhydrat
          </p>
          <p style={{left: "45%"}}>
          {nutrition.carbohydrate}
          </p>
        </div>
        <div className={'nutrition-line-powerade'}>
          <p style={{left: "5%"}}>
          varav sockerarter
          </p>
          <p style={{left: "45%"}}>
          {nutrition.sugars}
          </p>
        </div>
        <div className={'nutrition-line-powerade'}>
          <p style={{left: "0"}}>
          Protein
          </p>
          <p style={{left: "45%"}}>
          {nutrition.protein}
          </p>
        </div>
        <div className={'nutrition-line-powerade'}>
          <p style={{left: "0"}}>
          Natrium
          </p>
          <p style={{left: "45%"}}>
          {nutrition.natrium}
          </p>
        </div>
        <div className={'nutrition-line-bottomless-powerade'}>
          <p style={{left: "0"}}>
          VitaminB6
          </p>
          <p style={{left: "45%"}}>
          {nutrition.VitaminB6}
          </p>
          <strong style={{right: "0"}}>
          8%*
          </strong>
        </div>
      </div>
      )}
    </div>
  )
}

export default PoweradeNutritionSlide;