import React, { useEffect, useState } from 'react';

import Credits from './Credits.jsx';

import '../css/CreditSection.css';

const CreditSection = ({ movies, tv, combined, crew }) => {
  const [movieCredits, setMovieCredits] = useState([]);
  const [tvCredits, setTVCredits] = useState([]);
  const [combinedCredits, setCombinedCredits] = useState([]);
  const [chosenCredits, setChosenCredits] = useState([]);
  const [departments, setDepartments] = useState([]);
  const [movieYears, setMovieYears] = useState([]);
  const [tvSeriesYears, setTvSeriesYears] = useState([]);
  const [combinedYears, setCombinedYears] = useState([]);
  const [chosenYears, setChosenYears] = useState([]);
  const [creditName, setCreditName] = useState("");
  
  useEffect(() => {
    const movieYears = [];
    const tvSeriesYears = [];
    const combinedYears = [];
    const unknownDepartments = [];
    const sortedMovies = [...movies].sort((a, b) => {
      if(a.hasOwnProperty('release_date') && b.hasOwnProperty('release_date')) {
        return b.release_date.slice(0, 4) - a.release_date.slice(0, 4);
      }
      else {
        return null;
      }
    });
    const sortedTV = [...tv].sort((a, b) => {
      if(a.hasOwnProperty('first_air_date') && b.hasOwnProperty('first_air_date')) {
        return b.first_air_date.slice(0, 4) - a.first_air_date.slice(0, 4);
      }
      else {
        return null;
      }
    });
    const sortedCombined = [...combined].sort((a, b) => {
      if(a.hasOwnProperty('release_date') && b.hasOwnProperty('release_date')) {
        return b.release_date.slice(0, 4) - a.release_date.slice(0, 4);
      }
      if(a.hasOwnProperty('first_air_date') && b.hasOwnProperty('first_air_date')) {
        return b.first_air_date.slice(0, 4) - a.first_air_date.slice(0, 4);
      } 
      else {
        return null;
      }
    });

    for(const movie of movies) {
      if(movie.release_date !== undefined) {
        movieYears.push(movie.release_date.slice(0, 4));
      }
    }

    for(const tvSerie of tv) {
      if(tvSerie.first_air_date !== undefined) {
        tvSeriesYears.push(tvSerie.first_air_date.slice(0, 4));
      }
    }

    for(const comb of combined) {
      if(comb.hasOwnProperty('release_date') && comb !== undefined) {
        combinedYears.push(comb.release_date.slice(0, 4));
      }
      if(comb.hasOwnProperty('first_air_date') && comb !== undefined) {
        combinedYears.push(comb.first_air_date.slice(0, 4));
      }
    }

    const sortedMovieYears = [...movieYears].sort((a, b) => {
      return a - b;
    })

    const sortedTvSeriesYears = [...tvSeriesYears].sort((a, b) => {
      return a - b;
    })

    const sortedCombinedYears = [...combinedYears].sort((a, b) => {
      return a -  b;
    })

    for(const c of crew) {
      unknownDepartments.push(c.department);
    }

    const filteredMovies = sortedMovies.filter((el) => {
      return el.release_date !== undefined;
    });

    const filteredTV = sortedTV.filter((el) => {
      return el.first_air_date !== undefined
    });

    const filteredCombined = sortedCombined.filter((el) => {
      if(el.hasOwnProperty('release_date')) {
        return el.release_date !== undefined;
      }
      if(el.hasOwnProperty('first_air_date')) {
        return el.first_air_date !== undefined
      } else {
        return null;
      }
    });
    
    const uniqueMovieYears = Array.from(new Set(sortedMovieYears)).reverse();;
    const uniqueTvSeriesYears = Array.from(new Set(sortedTvSeriesYears)).reverse();;
    const uniqueCombinedYears = Array.from(new Set(sortedCombinedYears)).reverse();;
    const uniqueDepartments = Array.from(new Set(unknownDepartments));
    const uniqueTV = filteredTV.filter((v,i,a)=>a.findIndex(t=>(t.id === v.id))===i);
    const uniqueMovies = filteredMovies.filter((v,i,a)=>a.findIndex(t=>(t.id === v.id))===i);
    const uniqueCombined = filteredCombined.filter((v,i,a)=>a.findIndex(t=>(t.id === v.id))===i);

    /* First three seters are pre-picked in the initial run */ 
    setChosenCredits(uniqueCombined);
    setChosenYears(uniqueCombinedYears);
    setCreditName("Acting");
    setMovieYears(uniqueMovieYears);
    setTvSeriesYears(uniqueTvSeriesYears);
    setCombinedYears(uniqueCombinedYears);
    setMovieCredits(uniqueMovies);
    setTVCredits(uniqueTV);
    setCombinedCredits(uniqueCombined);
    setDepartments(uniqueDepartments)
  }, [movies, tv, combined, crew]);

  const handleDepartmentDisplay = (department) => {
    const chosenDepartment = [];
    const departmentYears = [];

    for(const c of crew) {
      if(c.department.toLowerCase() === department.toLowerCase()) {
        chosenDepartment.push(c);
      }
    }

    const sortedDepartment = [...chosenDepartment].sort((a, b) => {
      if(a.hasOwnProperty('release_date') && b.hasOwnProperty('release_date')) {
        return b.release_date.slice(0, 4) - a.release_date.slice(0, 4);
      }
      if(a.hasOwnProperty('first_air_date') && b.hasOwnProperty('first_air_date')) {
        return b.first_air_date.slice(0, 4) - a.first_air_date.slice(0, 4);
      } 
      else {
        return null;
      }
    });

    for(const department of chosenDepartment) {
      if(department.hasOwnProperty('release_date') && department !== undefined) {
        departmentYears.push(department.release_date.slice(0, 4));
      }
      if(department.hasOwnProperty('first_air_date') && department !== undefined) {
        departmentYears.push(department.first_air_date.slice(0, 4));
      }
    }

    const sortedDepartmentYears = [...departmentYears].sort((a, b) => {
      return a -  b;
    })

    const uniqueDepartment = sortedDepartment.filter((v, i, a) => a.findIndex(t=>(t.id === v.id)) === i);
    const uniqueDepartmentYears = Array.from(new Set(sortedDepartmentYears)).reverse();;

    setCreditName(department);
    setChosenYears(uniqueDepartmentYears);
    setChosenCredits(uniqueDepartment);
  }

  return (
    <div>
      <div className={'credit-departments row'}>
        <p onClick={() => {setChosenCredits(combinedCredits)
                                                          setChosenYears(combinedYears);
                                                          setCreditName("Acting")
                                                          }}>
                                                          Acting</p>
        <p onClick={() => {setChosenCredits(movieCredits)
                                                          setChosenYears(movieYears);
                                                          setCreditName("Acting in Movies")
                                                          }}>
                                                          Movies</p>
        <p onClick={() => {setChosenCredits(tvCredits)
                                                          setChosenYears(tvSeriesYears);
                                                          setCreditName("Acting in TV Series")
                                                          }}>
                                                          TV Series</p>
        <div className={'row'} style={{marginLeft: "0"}}>
          {departments.map((department, i) => (
            <p key={i} onClick={() => {handleDepartmentDisplay(department)}}>{department}</p>
          ))}
        </div>
      </div>
      <div style={{display: "flex", justifyContent: "center"}}>
        <p style={{color: "black"}}>Credits in</p>
        <p style={{color: "gold", marginLeft: "7px"}}>{creditName}</p>
      </div>
      <Credits credits={chosenCredits} years={chosenYears}/>
    </div>
  )
}

export default CreditSection;