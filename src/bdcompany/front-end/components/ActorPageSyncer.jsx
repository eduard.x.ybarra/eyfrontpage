import React, { useState, useEffect } from 'react';
import ActorPage from './ActorPage.jsx';
import axios from 'axios';

const API_KEY = "9ef196d7c4b8f349fad6bb4099995f8d";

const ActorPageSyncer = ({ id }) => {
  const [data, setData] = useState([]);
  const [movieCredits, setMovieCredtis] = useState([]);
  const [tvCredits, setTVCredtis] = useState([]);
  const [combinedCredits, setCombinedCredits] = useState([]);
  const [crew, setCrew] = useState([]);
  const [name, setName] = useState("");
  const [imagePath, setImagePath] = useState("");
  const [biography, setBiography] = useState("");
  const [department, setDepartment] = useState("");
  const [birthday, setBirthday] = useState("");
  const [birthplace, setBirthplace] = useState("");
  const [externalIds, setExternalIds] = useState([]);
  const [images, setImages] = useState([]);

  useEffect(() => {
    const fetchTMDBData = async () => {
      //https://api.themoviedb.org/3/person/6193/movie_credits?api_key=9ef196d7c4b8f349fad6bb4099995f8d&language=en-US
      await axios.all([
        axios.get(`https://api.themoviedb.org/3/person/${id}?api_key=${API_KEY}&language=en-US`),
        axios.get(`https://api.themoviedb.org/3/person/${id}/changes?api_key=${API_KEY}&page=1`),
        axios.get(`https://api.themoviedb.org/3/person/${id}/movie_credits?api_key=${API_KEY}&language=en-US`),
        axios.get(`https://api.themoviedb.org/3/person/${id}/tv_credits?api_key=${API_KEY}&language=en-US`),
        axios.get(`https://api.themoviedb.org/3/person/${id}/combined_credits?api_key=${API_KEY}&language=en-US`),
        axios.get(`https://api.themoviedb.org/3/person/${id}/external_ids?api_key=${API_KEY}&language=en-US`),
        axios.get(`https://api.themoviedb.org/3/person/${id}/images?api_key=${API_KEY}`)
      ])
        .then(axios.spread((data, changes, movieCredits, tvCredits, combinedCredits, externalIds, images) => {
          setData(data);
          setMovieCredtis(movieCredits.data.cast);
          setTVCredtis(tvCredits.data.cast);
          setCombinedCredits(combinedCredits.data.cast);
          setCrew(combinedCredits.data.crew);
          setName(data.data.name);
          setImagePath(data.data.profile_path);
          setBiography(data.data.biography);
          setDepartment(data.data.known_for_department);
          setBirthday(formateDate(data.data.birthday));
          setBirthplace(data.data.place_of_birth)
          setExternalIds(externalIds.data);
          setImages(images.data.profiles);
        }));
      }
    fetchTMDBData();
  }, [id]);

  const formateDate = (birthday) => {
    if(birthday !== null) {
      let day = birthday.substring(8, 10);
      let month = birthday.substring(5, 7);
      let year = birthday.substring(0, 4);
      const date = new Date(year, month, day);
      const monthName = date.toLocaleString('default', { month: 'long' });

      return monthName + " " + day + ", " + year;
    }
    return birthday;
  }

  return (
    <div>
      {data ? (
        <div>
          <ActorPage  externalIds={externalIds} 
                      movieCredits={movieCredits} 
                      tvCredits={tvCredits} 
                      combinedCredits={combinedCredits} 
                      crew={crew} 
                      name={name}
                      imagePath={imagePath} 
                      department={department} 
                      birthday={birthday} 
                      birthplace={birthplace} 
                      biography={biography} 
                      images={images}
                      />
        </div>  
      ) : (
        <div>
          Loading...
        </div>
      )}
    </div>
  )
}

export default ActorPageSyncer;