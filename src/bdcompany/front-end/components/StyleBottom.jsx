import React from 'react';
import '../css/Style.css';

const StyleBottom = ({ id, styleData, swapCompany }) => {

  const handleIncrement = () => {
    if(id >= styleData.length - 1) {
      swapCompany(0);
    } else {
      swapCompany(id + 1);
    }
  }

  const handleDecrement = () => {
    if(id <= 0) {
      swapCompany(styleData.length - 1)
    } else {
      swapCompany(id - 1)
    }
  }

  return (
    <div>
      <div className={"style-bottom-project-media"}>
        <div className={"style-bottom-wrapper"}>
          <h4>{styleData[id + 2 <= styleData.length ? (id + 1) : (0)].name}</h4>
          <div className={"style-media-frame-bottom"}>
            <video className={"img-fluid style-video-bottom"} src={require('../images/' + styleData[id + 2 <= styleData.length ? (id + 1) : (0)].video)} type="video/mp4" muted={true} autoPlay loop/>
            <span className={"style-media-text"} onClick={handleDecrement}>
              <p>Click to view project description</p>
            </span>
          </div>
          <strong onClick={handleDecrement}>View Project</strong>
        </div>
        <div className={"style-bottom-wrapper"}>
          <h4>{styleData[id + 2 < styleData.length ? (id + 2) : (id - 1)].name}</h4>
          <div className={"style-media-frame-bottom"}>
            <video className={"img-fluid style-video-bottom"} src={require('../images/' + styleData[id + 2 < styleData.length ? (id + 2) : (id - 1)].video)} type="video/mp4" muted={true} autoPlay loop/>
            <span className={"style-media-text"} onClick={handleIncrement}>
              <p>Click to view project description</p>
            </span>
          </div>
          <strong onClick={handleIncrement}>View Project</strong>
        </div>
      </div>
    </div>

  )
}

export default StyleBottom;