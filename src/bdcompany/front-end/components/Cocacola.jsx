import React, { useState, useEffect } from 'react';

import CocaColaDrinks from './CocacolaDrinks.jsx';
import CocaColaBottomSection from './CocaColaBottomSection.jsx';

import '../css/Cocacola.css';

import background from '../cocacola-images/cocacola/cocacola-top-background.jpeg';
import ccbg from '../cocacola-images/cocacola/cc-bottom-bg.png';
import energyBg from '../cocacola-images/cocacola/cocacola-energy-background.jpeg';
import lightBg from '../cocacola-images/cocacola/cocacola-light-background.jpeg';
import cocacolaBg from '../cocacola-images/cocacola/cocacola-original-background.jpeg';
import zeroBg from '../cocacola-images/cocacola/cocacola-zero-background.jpeg';
import signatureBg from '../cocacola-images/cocacola/cocacola-signature-background.jpeg';
import cherry from '../cocacola-images/cocacola/cocacola-cherry.jpeg';
import lemon from '../cocacola-images/cocacola/cocacola-lemon.jpeg';
import original from '../cocacola-images/cocacola/cocacola-original.jpeg';
import vanilla from '../cocacola-images/cocacola/cocacola-vanilla.jpeg';
import energy from '../cocacola-images/cocacola/cocacola-energy.jpeg';
import energyLight from '../cocacola-images/cocacola/cocacola-energy-light.jpeg';
import signatureSmoky from '../cocacola-images/cocacola/cocacola-signature-smoky.jpeg'; 
import signatureHerbal from '../cocacola-images/cocacola/cocacola-signature-herbal.jpeg'; 
import signatureSpicy from '../cocacola-images/cocacola/cocacola-signature-spicy.jpeg'; 
import signatureWoody from '../cocacola-images/cocacola/cocacola-signature-woody.jpeg'; 
import light from '../cocacola-images/cocacola/cocacola-light.jpeg'; 
import zero from '../cocacola-images/cocacola/cocacola-zero.png'; 
import zeroVanilla from '../cocacola-images/cocacola/cocacola-zero-vanilla.jpeg'; 
import zeroRaspberry from '../cocacola-images/cocacola/cocacola-zero-raspberry.png'; 

const backgrounds = [cocacolaBg, lightBg, zeroBg, energyBg, signatureBg];
const energyDrinks = [energy, energyLight];
const lightDrinks = [light];
const originalDrinks = [original, cherry, vanilla, lemon];
const zeroDrinks = [zero, zeroVanilla, zeroRaspberry];
const signatureDrinks = [signatureSmoky, signatureSpicy, signatureHerbal, signatureWoody];
const collectedDrinks = [originalDrinks, lightDrinks, zeroDrinks, energyDrinks, signatureDrinks];

const Cocacola = ({ cocacolaNutritionData }) => {
  const [selectedDrink, setSelectedDrink] = useState([]);

  useEffect(() => {
    let kindsOfDrinks = [];

    for(let i = 0; i < cocacolaNutritionData.length; i++) {
      kindsOfDrinks.push(null);
    }

    setSelectedDrink(kindsOfDrinks);
  }, [cocacolaNutritionData])

  const handleDropdownOnClick = (dataId, tasteId) => {
    let yPosition = dataId * 680 + 500;
    let newSelectedDrink = selectedDrink;

    newSelectedDrink[dataId] = tasteId;
    window.scrollTo(0, yPosition);
    setSelectedDrink([...newSelectedDrink]);
  }

  return (
    <div style={{position: "relative"}}>
      <img src={background} alt={'Coca-Cola'} style={{width: "100%", height: "500px", objectFit: "cover"}}/>
      <div className={'cocacola-container'}>
        <ul>
        {cocacolaNutritionData.map((data) => (
          <li className={'cocacola-dropdown-list'} key={data.id} style={{background: data.background, color: data.color}}>
            <p className={'cocacola-dropdown-names'} onClick={() => handleDropdownOnClick(data.id, null)}>{data.name} &#9662;</p>
            <ul className={'cocacola-dropdown'}>
            {data.variations.map((taste) => (
              <li key={taste.id} onClick={() => handleDropdownOnClick(data.id, taste.id)}>
                <div style={{background: data.background}}>
                  <p className={'cocacola-dropdown-names'} style={{color: data.color, borderRight: `5px solid ${taste.color}`, borderLeft: `5px solid ${taste.color}`}}>{taste.title}</p>
                </div>
              </li>
            ))}
            </ul>
          </li>
        ))}
        </ul>
      </div>
      <div>
      {cocacolaNutritionData.map((data) => (
        <CocaColaDrinks key={data.id} data={data} collectedDrinks={collectedDrinks[data.id]} backgrounds={backgrounds[data.id]} selectedDrink={selectedDrink}/>
      ))}
      </div>
      <div className={'cocacola-fact-text'}>
        <img src={ccbg} alt={'original-logo'}/>
        <p>
        Coca‑Cola är världens mest välkända och älskade läskedryck
        </p>
      </div>
      <CocaColaBottomSection />
    </div>
  )
}

export default Cocacola;