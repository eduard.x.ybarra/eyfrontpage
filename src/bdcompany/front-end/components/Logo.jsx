import React, { useState, useEffect } from 'react';
import logo from '../images/BDlogo.svg';
import logotext from '../images/BDtext.svg';
import '../css/Logo.css';

const Logo = ({ scroll, showLogo }) => {
  const [showIcon, setShowIcon] = useState(true);

  useEffect(() => {
    if(scroll <= 0 && showLogo) {
      setShowIcon(true);
    } else {
      setShowIcon(false)
    }
  }, [scroll, showLogo]);

  return (
    <div className={"row image-resize-background"} style={{height: showIcon ? ("270px") : ("0px"), backgroundColor: showIcon ? ("transparent") : ("black")}}>
      <img className={"center-block image-resize"} style={{display: showIcon ? ("") : ("none"), opacity: showIcon ? ("1") : ("0")}} src={logo} alt="Budatch Design"></img>
      <div className={"target-dummy-logo"} style={{display: showIcon ? ("") : ("none"), opacity: showIcon ? ("1") : ("0")}}/>
      <div className={"target-dummy-logo"} style={{display: showIcon ? ("") : ("none"), opacity: showIcon ? ("1") : ("0")}}/>
      <img className={"center-block image-resize"} style={{display: showIcon ? ("") : ("none"), opacity: showIcon ? ("1") : ("0")}} src={logotext} alt="Budatch Design"></img>
    </div>
  )
}

export default Logo;