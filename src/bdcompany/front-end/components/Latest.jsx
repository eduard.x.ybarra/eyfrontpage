import React from 'react';
import { useNavigate } from 'react-router-dom';

import HeaderVideo from './HeaderVideo.jsx';
import HeaderVideoAssister from './HeaderVideoAssister.jsx';
import LatestImageBox from './LatestImageBox.jsx';

import latest from '../images/Latest.mp4';

const Latest = ({ latestData }) => {
  const navigate = useNavigate();

  return (
    <div>
      <HeaderVideo video={latest} path={"Latest"}/>
      <div className={"latest-box"}>
        <div className={"row justify-content-center"} style={{background: "black", width: "80vw", padding: "3em"}}>
          {latestData.map(latest => (
            <LatestImageBox key={latest.id} onClick={() => navigate(`/latest/articles`)} picture={latest.picture} name={latest.name} pictureWidth={latest.pictureWidth}/>
          ))}
        </div>
      </div>
    </div>
  )
}

export default Latest;