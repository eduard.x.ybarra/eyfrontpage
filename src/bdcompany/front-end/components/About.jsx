import React, { useState, useEffect, useRef } from 'react';
import HeaderVideo from './HeaderVideo.jsx';
import '../css/About.css';
import about from '../images/About.mp4';

function useInterval(callback, delay) {
  const savedCallback = useRef();

  useEffect(() => {
    savedCallback.current = callback;
  }, [callback]);

  useEffect(() => {
    function tick() {
      savedCallback.current();
    }
    if (delay !== null) {
      let id = setInterval(tick, delay);
      return () => clearInterval(id);
    }
  }, [delay]);
}

const About = ({ aboutData, aboutQuotesData }) => {
  const[randomQuote, setRandomQuote] = useState(aboutQuotesData[0]);

  const nextQuote = () => {
    let quoteId = 0;

    if(aboutQuotesData.length > quoteId + 1) {
      quoteId = randomQuote.id + 1;
    }

    if(aboutQuotesData.length === randomQuote.id + 1) {
      quoteId = 0;
    }

    setRandomQuote(aboutQuotesData[quoteId]);
  }

  useInterval(nextQuote, 3000);

  return (
    <div>
      <HeaderVideo video={about} path={"About"}/>
      <div className={"about-quote-box"}>
        <div className={"quote-text row justify-content-center"}>
          <span>{randomQuote.text}</span>
        </div>
      </div>
      <div className={"d-flex"}>
        <div className={"separator-line"}/>
        <div className={"separator-line-reversed"}/>
      </div>
      <div className={"about-team"}>
        {aboutData.map(contact => (
          <div className={"about-member"} key={contact.id}>
            <div className={`${contact.id % 2 === 0 ? ("about-member-left") : ("about-member-right-reversed")}`}>
              {contact.id % 2 === 0 ? (
                  <div className={"about-member-image"}>
                    <img src={require('../images/' + contact.picture)} alt={contact.name}/>
                  </div>
              ) : (
                <div className={"about-member-information-title"}>
                  <strong>{contact.name}</strong>
                  <strong>{contact.title}</strong>
                  <div className={"about-member-information"}>
                    <p>{contact.description}</p>
                  </div>
                  <div className={"about-member-information-contact"}>
                    <p>{contact.phone}</p>
                    <p>{contact.email}</p>
                  </div>
                </div>
              )}
            </div>
            <div className={`${contact.id % 2 !== 0 ? ("about-member-left-reversed") : ("about-member-right")}`}>
              {contact.id % 2 !== 0 ? (
                  <div className={"about-member-image"}>
                    <img src={require('../images/' + contact.picture)} alt={contact.name}/>
                  </div>
              ) : (
                <div className={"about-member-information-title"}>
                  <strong>{contact.name}</strong>
                  <strong>{contact.title}</strong>
                  <div className={"about-member-information"}>
                    <p>{contact.description}</p>
                  </div>
                  <div className={"about-member-information-contact"}>
                    <p>{contact.phone}</p>
                    <p>{contact.email}</p>
                  </div>
                </div>
              )}
            </div>
          </div>
        ))}
      </div>
    </div>
  )
}

export default About;