import React from 'react';
import { useNavigate } from 'react-router-dom';
import '../css/DropdownList.css';

const imageURL = "https://image.tmdb.org/t/p/w200";

const DropdownList = ({ data, handlePersonId }) => {
  const navigate = useNavigate();

  const handleOnClick = (personName, personId) => {
    handlePersonId(personId);
    navigate(`${personName}-${personId}`);
  }
  
  return (
    <div>
      <ul className={"dropdown-list-group"} style={{height: data.length === 1 ? ("100px") : ("150px"), marginBottom: data.length === 1 ? ("") : ("-35px"), listStyleType: "none"}}>
        {data.map((person, index) => (
        <li key={person.id} className={"dropdown-list-group-item"} style={{display: person.profile_path ? ("") : ("none")}} onClick={() => handleOnClick(person.name, person.id)} name={person.name + person.id}>
          <p>
            <img src={imageURL + person.profile_path} alt={person.name}/>
            {person.name}
          </p>
        </li>
        ))}
      </ul>
    </div>
  )
}

export default DropdownList;