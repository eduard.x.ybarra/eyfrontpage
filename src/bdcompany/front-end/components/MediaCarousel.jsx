import React, { useEffect, useState } from 'react';
import styled, { keyframes } from 'styled-components'
import filmroll from '../images/filmroll.png';
import '../css/MediaCarousel.css';

const imagePath = "http://image.tmdb.org/t/p/w200/";

const animation = (props) => `
    infinite ${props.seconds}s alternate
`;

const slide = (props) => keyframes`
    0% {
      margin-left: 0%;
    }
    100% {
      margin-left: -${props.pixel}em;
    }
`;

const height = (props) => `
    height: ${props.height};
`;

const List = styled.ul`
    ${height}
    padding: 0;
    display: flex;
    overflow: hidden;
    margin-bottom: 0;
    animation: ${slide} ${animation};
    &:hover {
      animation-play-state: paused;
    }
    transition:  height 0.5s ease;
`;

const ListItem = styled.li`
    list-style-type: none;
`;

const MediaCarousel = ({ movies, hideCarousel }) => {
  const [pixel, setPixel] = useState(0);
  const [seconds, setSeconds] = useState(0);
  const [height, setHeight] = useState("18.75em");

  useEffect(() => {
    if(movies.length > 10) {
      setPixel(movies.length * 8);
      setSeconds(movies.length);
    }
  }, [movies]);

  useEffect(() => {
    if(hideCarousel) {
      setHeight("18.75em");
    } else {
      setHeight("0");
    }
  }, [hideCarousel])

  return (
    <div>
      <List pixel={pixel} seconds={seconds} height={height}>
      {movies.map((movie) => (
        <ListItem key={movie.id}>
          <div className={'media-carousel-container'}>
            <img className={'filmroll-image'} src={filmroll} alt={"filmroll"} style={{width: "12.5em", height: "18.75em"}} />
            <img className={'poster-image'} src={imagePath + movie.poster} alt={movie.name} style={{width: "9.375em", height: "14.0625em"}}/>
          </div>
        </ListItem>
      ))}
      </List>
    </div>
  )
}

export default MediaCarousel;