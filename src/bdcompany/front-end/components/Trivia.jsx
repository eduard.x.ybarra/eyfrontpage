import React from 'react';

import '../css/Trivia.css';

const Trivia = ({ trivia, triviaIndex, trivias, handleOnClickTriva }) => {

  const handleOnClick = (index) => {
    handleOnClickTriva(index);
  }

  return (
    <div className={'trivia-container'}>
      <div>
        {trivia ? (
          <div style={{display: "flex"}}>
            <p>Trivia:</p>
            <p>{triviaIndex + 1} / {trivias.length}</p>
            <i className="fa fa-angle-left fa-sm" onClick={() => handleOnClick(triviaIndex - 1)}></i>
            <i className="fa fa-angle-right fa-sm" onClick={() => handleOnClick(triviaIndex + 1)}></i>
          </div>
        ) : (<p>Loading trivias...</p>)}
      </div>
      <div className={'trivia-paragraph'}>
        <p>{trivia}</p>
      </div>
    </div>
  )
}

export default Trivia;