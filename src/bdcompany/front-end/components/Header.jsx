import React, { useState, useEffect } from 'react';
import { Link, useLocation } from "react-router-dom";
import Logo from './Logo.jsx';
import '../css/Header.css';

const Header = () => {
  const [scroll, setScroll] = useState(window.pageYOffset);
  const [currentLink, setCurrentLink] = useState("");
  const [headerTransparency, setHeaderTransparency] = useState(true);
  const [showLogo, setShowLogo] = useState(true);
  
  useEffect(() => {
    window.addEventListener("scroll", handleScroll);

    return () => window.removeEventListener("scroll", handleScroll);
  }, [])
  
  useEffect(() => {
    if(scroll <= 0 && showLogo !== false) {
      setHeaderTransparency(true)
    } else {
      setHeaderTransparency(false)
    }
  }, [scroll])

  const handleScroll = () => {
    setScroll(window.pageYOffset);
  }

  function usePageViews() {
    let location = useLocation();
    useEffect(() => {
      if(location.pathname.includes('budatchprojects')){
        setHeaderTransparency(false);
        setShowLogo(false);
      } else {
        setHeaderTransparency(true)
        setShowLogo(true);
      }
      setCurrentLink(location.pathname);
    }, [location]);
  }

  usePageViews();

  return (
    <React.Fragment>
      <header className={"header fixed-top"}>
        <Logo scroll={scroll} showLogo={showLogo}/>
        <div>
            <div className={"links"} style={{backgroundColor: headerTransparency ? ("transparent") : ("black")}}>
              <Link className={"link"} to="">
                <h6 className={currentLink === ("/budatchdesign") ? ("header-link-selected") : ("header-link-unselected")}>HOME</h6>
              </Link>
              <Link className={"link"} to="/budatchdesign/about">
                <h6 className={currentLink === ("/budatchdesign/about") ? ("header-link-selected") : ("header-link-unselected")}>ABOUT</h6>
              </Link>
              <Link className={"link"} to="/budatchdesign/style">
                <h6 className={currentLink === ("/budatchdesign/style") ? ("header-link-selected") : ("header-link-unselected")}>STYLE</h6>
              </Link>
              <Link className={"link"} to="/budatchdesign/latest">
                <h6 className={currentLink === ("/budatchdesign/latest") ? ("header-link-selected") : ("header-link-unselected")}>LATEST</h6>
              </Link>
              <Link className={"link"} to="/budatchdesign/contact">
                <h6 className={currentLink === ("/budatchdesign/contact") ? ("header-link-selected") : ("header-link-unselected")}>CONTACT</h6>
              </Link>
              <Link className={"link"} to="/budatchdesign/budatchprojects">
                <h6 className={currentLink === ("/budatchdesign/budatchprojects") ? ("header-link-selected") : ("header-link-unselected")}>PROJECTS</h6>
              </Link>
            </div>
          </div>
      </header>
    </React.Fragment>
  )
}

export default Header;