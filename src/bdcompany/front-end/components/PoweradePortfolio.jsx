import React, { useState, useEffect } from 'react';

import PoweradePortfolioImage from './PoweradePortfolioImage.jsx';

import '../css/PoweradePortfolio.css';

import img1 from '../cocacola-images/powerade/portfolio/powerade-image-one.jpeg';
import img2 from '../cocacola-images/powerade/portfolio/powerade-image-two.jpeg';
import img3 from '../cocacola-images/powerade/portfolio/powerade-image-three.jpeg';
import img4 from '../cocacola-images/powerade/portfolio/powerade-image-four.jpeg';
import img5 from '../cocacola-images/powerade/portfolio/powerade-image-five.jpeg';
import img6 from '../cocacola-images/powerade/portfolio/powerade-image-six.jpeg';
import img7 from '../cocacola-images/powerade/portfolio/powerade-image-seven.jpeg';
import img8 from '../cocacola-images/powerade/portfolio/powerade-image-eight.jpeg';
import img9 from '../cocacola-images/powerade/portfolio/powerade-image-nine.jpeg';
import img10 from '../cocacola-images/powerade/portfolio/powerade-image-ten.jpeg';
import img11 from '../cocacola-images/powerade/portfolio/powerade-image-11.jpeg';
import img12 from '../cocacola-images/powerade/portfolio/powerade-image-12.jpeg';
import img13 from '../cocacola-images/powerade/portfolio/powerade-image-13.jpeg';
import img14 from '../cocacola-images/powerade/portfolio/powerade-image-14.jpeg';
import img15 from '../cocacola-images/powerade/portfolio/powerade-image-15.jpeg';
import img16 from '../cocacola-images/powerade/portfolio/powerade-image-16.jpeg';

let portfolioImages = [img1, img2, img3, img4, img5, img6, img7, img8, img9, img10, img11, img12, img13, img14, img15, img16];
let starterImages = [img7, img4, img3, img11];

const PoweradePortfolio = ({ bgLogo }) => {
  const[images, setImages] = useState(starterImages);

  useEffect(() => {
    const timer = setInterval(() => {
      portfolioImageRandomizer();
    }, 2500);
    return () => clearInterval(timer);
  }, []);

  const portfolioImageRandomizer = () => {
    let stateImages = starterImages;
    let imagesReplica = portfolioImages;
    imagesReplica = imagesReplica.filter(val => !starterImages.includes(val));
    
    stateImages[(Math.floor(Math.random() * stateImages.length))] = imagesReplica[(Math.floor(Math.random() * imagesReplica.length))];
    setImages([...stateImages])
  }

  return (
    <div className={'powerade-portfolio-container'}>
      <div className={'powerade-portfolio-logo-background'} style={{backgroundImage: `url(${bgLogo})`}}>
        <div className={'portfolio-background'}>
          <div className={'portfolio-images'}>
            <PoweradePortfolioImage image={images[0]} />
            <PoweradePortfolioImage image={images[1]} />
            <PoweradePortfolioImage image={images[2]} />
            <PoweradePortfolioImage image={images[3]} />
          </div>
          <div className={'more-from-swanson'}>
            <a className={'row'} href={"https://www.swansonstudio.us/work/powerade"} target="_blank" rel="noopener noreferrer" >
              <p>View more from Swansonstudio</p>
              <i className="fa fa-caret-right"></i>
            </a>
          </div>  
        </div>
      </div>
    </div>
  )
}

export default PoweradePortfolio;