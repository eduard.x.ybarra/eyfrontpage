import React, { useState, useEffect } from 'react';
import { CSSTransition, SwitchTransition } from "react-transition-group";

import '../css/CocacolaDrinks.css';

const CocaColaDrinks = ({ data, collectedDrinks, backgrounds, selectedDrink }) => {
  const [id, setId] = useState(null);
  const [variations, setVariations] = useState([]);
  const [text, setText] = useState(data.description);

  useEffect(() => {
    if(selectedDrink[data.id] !== undefined) {
      if(selectedDrink[data.id] !== null) {
        setId(selectedDrink[data.id]);
      } else {
        setId(null);
      }
    }
  }, [data, selectedDrink]);

  useEffect(() => {
    let kindsOfVariations = [];
    
    for(let i = 0; i < data.variations.length; i++) {
      kindsOfVariations.push(data.variations[i]);
    }

    setVariations(kindsOfVariations);
  }, [data]);

  useEffect(() => {
    if(id === null) {
      setText(data.description);
    } else {
      setText(data.variations[id].description);
    }
  }, [id, data, collectedDrinks]);

  const handleOnClick = (tasteId) => {
    setId(tasteId);
  }

  const inViewport = (entries, observer) => {
    entries.forEach(entry => {
      entry.target.classList.toggle("is-inViewport", entry.isIntersecting);
    });
  };
  
  const Obs = new IntersectionObserver(inViewport);
  const obsOptions = {};
  
  const elements_inViewport = document.querySelectorAll('[data-inviewport]');
    elements_inViewport.forEach(element => {
      Obs.observe(element, obsOptions);
  });

  return (
    <div style={{overflowX: "hidden"}}>
      <div className={'cocacola-parallax-image row justify-content-center'} style={{backgroundImage: `url(${backgrounds})`}}>
      <SwitchTransition mode={"out-in"}>
        <CSSTransition
          classNames="fade"
          addEndListener={(node, done) => {
            node.addEventListener("transitionend", done, false);
          }}
          key={text}
        >
          <p className="cocacola-text">{text}</p>
        </CSSTransition>
      </SwitchTransition>
      </div>
      <div data-inviewport="slide-in">
      {id === null ? (
        <div className={'cocacola-drink-container'}>
        {variations.map((drink => (
          <div key={drink.id} className={'cocacola-drink'}>
            <img src={collectedDrinks[drink.id]} alt={'Coca-Cola'} onClick={() => handleOnClick(drink.id)}/>
          </div>
        )))}
        </div>
      ) : (
        <div className={'cocacola-drink-container'}>
          <div>
            <div className={'cocacola-nutrition'}>
              <div className={'cocacola-nutrition-wrapper'}>
                <div className={'cocacola-nutrition-text row'}>
                  <strong>Näringsvärde</strong>
                  <strong>per 100 ml</strong>
                </div>
              </div>
              <div className={'cocacola-nutrition-wrapper'}>
                <div className={'cocacola-nutrition-text row'}>
                  <strong>Kalorier</strong>
                  <strong>{variations[id].energi}</strong>
                </div>
              </div>
              <div className={'cocacola-nutrition-wrapper'} style={{borderBottom: "none"}}>
                <div className={'cocacola-nutrition-text row'}>
                  <strong>Fett</strong>
                  <p>{variations[id].fat}</p>
                </div>
              </div>
              <div className={'cocacola-nutrition-wrapper'}>
                <div className={'cocacola-nutrition-text row'}>
                  <strong style={{marginLeft: "20px"}}>varav mättat fett</strong>
                  <p>{variations[id].saturatedFat}</p>
                </div>
              </div>
              <div className={'cocacola-nutrition-wrapper'} style={{borderBottom: "none"}}>
                <div className={'cocacola-nutrition-text row'}>
                  <strong>Kolhydrat</strong>
                  <p>{variations[id].carbohydrate}</p>
                </div>
              </div>
              <div className={'cocacola-nutrition-wrapper'}>
                <div className={'cocacola-nutrition-text row'}>
                  <strong style={{marginLeft: "20px"}}>varav sockerarter</strong>
                  <p>{variations[id].sugars}</p>
                </div>
              </div>
              <div className={'cocacola-nutrition-wrapper'}>
                <div className={'cocacola-nutrition-text row'}>
                  <strong>Protein</strong>
                  <p>{variations[id].protein}</p>
                </div>
              </div>
              <div className={'cocacola-nutrition-wrapper'}>
                <div className={'cocacola-nutrition-text row'}>
                  <strong>Sodium</strong>
                  <p>{variations[id].sodium}</p>
                </div>
              </div>
            </div>
            <div className={'cocacola-drink'}>
              <img src={collectedDrinks[id]} alt={'Coca-Cola'} onClick={() => handleOnClick(null)}/>
            </div>
            <div className={'cocacola-ingredients'}>
              <strong className={'cocacola-ingredient-title'}>Ingredients</strong>
              <div className={'cocacola-ingredient-container-one'}>
                <div className={'cocacola-ingredient-angle-one'}/>
              </div>
              <div className={'cocacola-ingredient-container-text'}>
                <p>{variations[id].ingredients}</p>
              </div>
              <div className={'cocacola-ingredient-container-two'}>
                <div className={'cocacola-ingredient-angle-two'}/>
              </div>
            </div>
          </div>
        </div>
      )}
      </div>
    </div>
  )
}

export default CocaColaDrinks;