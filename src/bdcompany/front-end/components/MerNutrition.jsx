import React from 'react';
import '../css/MerNutrition.css';

const MerNutrition = ({ merNutritionData }) => {

  return (
    <div className={'fade-in-nutrition row justify-content-between'} >
      <div className={'nutrition-container'}>
        <div className={'nutrition-information'}>
          <strong>MER {merNutritionData.title}</strong>
          <p>{merNutritionData.description}</p>
        </div>
        <div style={{borderBottom: "solid black 1px", width: "350px", margin: "0 auto"}}/>
        <div className={'nutrition-information'}>
          <strong>Ingredienser</strong>
          <p>{merNutritionData.ingredients}</p>
        </div>
      </div>
      <div className={'nutrition-container'}>
        <div className={'nutrition-line'}>
          <strong style={{left: "0"}}>
          Näringsvärde
          </strong>
          <strong style={{right: "0"}}>
          per 100 ml
          </strong>
        </div>
        <div className={'nutrition-line'}>
          <strong style={{left: "0"}}>
          Kalorier
          </strong>
          <strong style={{right: "0"}}>
          {merNutritionData.energi}
          </strong>
        </div>
        <div className={'nutrition-line-bottomless'}>
          <p style={{left: "0"}}>
          Fett
          </p>
          <p style={{left: "45%"}}>
          {merNutritionData.fat}
          </p>
        </div>
        <div className={'nutrition-line'}>
          <p style={{left: "5%"}}>
          varav mättat fett
          </p>
          <p style={{left: "45%"}}>
          {merNutritionData.saturatedFat}
          </p>
        </div>
        <div className={'nutrition-line-bottomless'}>
          <p style={{left: "0"}}>
          Kolhydrat
          </p>
          <p style={{left: "45%"}}>
          {merNutritionData.carbohydrate}
          </p>
        </div>
        <div className={'nutrition-line'}>
          <p style={{left: "5%"}}>
          varav sockerarter
          </p>
          <p style={{left: "45%"}}>
          {merNutritionData.sugars}
          </p>
        </div>
        <div className={'nutrition-line'}>
          <p style={{left: "0"}}>
          Protein
          </p>
          <p style={{left: "45%"}}>
          {merNutritionData.protein}
          </p>
        </div>
        <div className={'nutrition-line-bottomless'}>
          <p style={{left: "0"}}>
          Sodium
          </p>
          <p style={{left: "45%"}}>
          {merNutritionData.sodium}
          </p>
        </div>
      </div>
    </div>
  )
}

export default MerNutrition;