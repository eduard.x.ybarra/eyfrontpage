import React from 'react';

import '../css/CocacolaNutrition.css';

const CocaColaNutrition = ({ image }) => {
  return (
    <React.fragment>
      <div>
        <img src={image} alt={'Coca-Cola'} onClick={() => handleOnClick(null)}/>
      </div>
      <div className={'one-drink'} onClick={() => handleOnClick(null)}>
        <i className="show-drinks arrow-right fa fa-angle-left fa-stack-1x"></i>
      </div>
    </React.fragment>
  )
}

export default CocaColaNutrition;