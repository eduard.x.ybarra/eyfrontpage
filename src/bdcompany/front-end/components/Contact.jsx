import React from 'react';

import HeaderVideo from './HeaderVideo.jsx';
import HeaderVideoAssister from './HeaderVideoAssister.jsx';

import '../css/Contact.css';

import contact from '../images/Contact.mp4';

const Contact = () => {

  return (
    <div>
      <HeaderVideo video={contact} path={"Contact"} />
      <div className={"contact-box"} style={{height: "420px", backgroundColor: "white"}}>
        <div className={"quote-text row justify-content-center"}>
          <span>
          Budatch Design is all about building friendships, and that starts with building a great community. We would love to share what we’re doing as we grow our company, so please drop by and say hello.

          We are still in our early stages. We’re taking the time to build our culture and our voice, so we can create great platform that bring people together. Please be patient with us, and we will share details as soon as we can. We can’t wait to go on this journey with you.

          Budatch Design is located in Jordbro, Stockholm.
          </span>
        </div>
      </div>
    </div>
  )
}

export default Contact;