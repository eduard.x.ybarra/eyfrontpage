import React from 'react';

import SeperatorLineImage from '../images/BD-seperator-long.png'; 

import '../css/SeperatorLine.css';

const imageRepeater = 19;

const SeperatorLine = ({ moveRight }) => {

  return (
    <div className={'seperator-line-container'} style={{transform: moveRight ? ('scaleX(-1)') : (null)}}>
      {[...Array(imageRepeater)].map((elementInArray, index) => (
        <img key={index} src={SeperatorLineImage} className={`seperator-line-bd`}/>
      ))}
    </div>
  )
}

export default SeperatorLine;