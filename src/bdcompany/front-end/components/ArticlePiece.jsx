import React from 'react';

const ArticlePiece = ({ latest }) => {
  return (
    <div className={"d-flex "}>
      <img src={require('../images/' + latest.picture)} alt={latest.name} />
      <div className={"article-name"}>
        <p>{latest.name}</p>
      </div>
    </div>
  )
}

export default ArticlePiece;