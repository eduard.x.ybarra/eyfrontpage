import React, { useState } from 'react';
import { useNavigate } from 'react-router-dom';
import logotext from '../images/BDtext.svg';
import cocacolaofficialbg from '../cocacola-images/cocacolaofficialbg.png';
import logococacola from '../cocacola-images/logococacola.png';
import logomer from '../cocacola-images/logomer.png';
import logopowerade from '../cocacola-images/logopowerade.png';
import cocacolaofficial from '../cocacola-images/cocacolaofficial.png';
import merofficial from '../cocacola-images/merofficial.png';
import poweradeofficial from '../cocacola-images/poweradeofficial.png';
import bottomcocacola from '../cocacola-images/cc-bottom/bottom-cocacola.png';
import bottommer from '../cocacola-images/cc-bottom/bottom-mer.png';
import bottompowerade from '../cocacola-images/cc-bottom/bottom-powerade.png';
import '../css/ProjectsCocacola.css';

const ProjectsCocacola = () => {
  const[showOfficial, setShowOfficial] = useState(false);

  const navigate = useNavigate();

  const handleOnClick = () => {
    setShowOfficial(!showOfficial)
  }

  return (
    <div className={'projects-rework'} style={{height: showOfficial ? ("1000px") : ("520px")}}>
      <div className={"reworked-cocacola"} style={{borderBottom: "solid black 1px"}}>
        <div className={"logo-capsulated"}>
          <img src={logococacola} alt={"cocacolalogo"}/>
        </div>
        <div className={"logo-capsulated"}>
          <img src={logopowerade} alt={"poweradelogo"} style={{height: "80px"}} />
        </div>
        <div className={"logo-capsulated"}>
          <img src={logomer} alt={"merlogo"}/>
        </div>
      </div>
      <div className={"company-logo-budatchdesign"}>
        <img className={"budatch-design-logotext"} src={logotext} alt="budatch design"></img>
      </div>
      <div className={"reworked-cocacola"} style={{backgroundColor: "rgb(215, 190, 95)", height: "300px", position: "relative", zIndex: "1"}}>
        <div className={"reworked-design"}>
          <img src={bottomcocacola} onClick={() => navigate(`coca-cola`)} alt="official cocacola"/>
          <p onClick={() => navigate(`coca-cola`)}>View Coca-Cola Rework</p>
        </div>
        <div className={"reworked-design"}>
          <img src={bottompowerade} onClick={() => navigate(`powerade`)} alt="official cocacola"/>
          <p onClick={() => navigate(`powerade`)}>View Powerade Rework</p>
        </div>
        <div className={"reworked-design"}>
          <img src={bottommer} onClick={() => navigate(`mer`)} alt="official cocacola"/>
          <p onClick={() => navigate(`mer`)}>View Mer Rework</p>
        </div>
      </div>
      <div className={"show-official"} onClick={() => handleOnClick()}>
        {showOfficial ? (
          <p>Show Official Coca-Cola Websites</p>
          ) : (
          <p>Hide Official Coca-Cola Websites</p>
        )}
        {showOfficial ? (
          <i className="fa fa-angle-up fa-sm"></i>
          ) : (
          <i className="fa fa-angle-down fa-sm"></i>
        )}
      </div>
      <div className={`${showOfficial ? ("slider-show") : ("")} show-official-slider`}>
        <div className={"company-logo-cocacola"}>
          <img className={"official-cocacola-logotext"} src={cocacolaofficialbg} alt="official cocacola"></img>
        </div>
        <div className={"reworked-cocacola"} style={{backgroundColor: "#f40001", height: "300px"}}>
          <div className={"official-cocacola"}>
            <img onClick={() => window.location.href = "https://www.coca-cola.se/vara-varumarken/coca-cola"} src={cocacolaofficial} alt="official cocacola"/>
            <p onClick={() => window.location.href = "https://www.coca-cola.se/vara-varumarken/coca-cola"}>View Official Website</p>
          </div>
          <div className={"official-cocacola"}>
            <img onClick={() => window.location.href = "https://www.coca-cola.se/vara-varumarken/powerade"} src={poweradeofficial} alt="official cocacola"/>
            <p onClick={() => window.location.href = "https://www.coca-cola.se/vara-varumarken/powerade"}>View Official Website</p>
          </div>
          <div className={"official-cocacola"}>
            <img onClick={() => window.location.href = "https://www.coca-cola.se/vara-varumarken/mer"} src={merofficial} alt="official cocacola"/>
            <p onClick={() => window.location.href = "https://www.coca-cola.se/vara-varumarken/mer"}>View Official Website</p>
          </div>
        </div>
      </div>
    </div>
  )
}

export default ProjectsCocacola;