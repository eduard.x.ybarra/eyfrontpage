import React, { useState, useEffect } from 'react';

const PoweradePortfolioImage = ({ image }) => {
  const[blur, setBlur] = useState(true);
  
  useEffect(() => {
    let timer1 = setTimeout(() => setBlur(false), 500);
    return () => {
      clearTimeout(timer1);
      setBlur(true);
    };
  }, [image])

  return (
    <React.Fragment>
      <img src={image} alt={'powerade-com'} style={{filter: blur ? ("blur(20px)") : (null)}}/>
    </React.Fragment>
  )
}

export default PoweradePortfolioImage;