import React from 'react';
import '../css/Powerade.css';
import PoweradeNutritionSlide from './PoweradeNutritionSlide.jsx';
import PoweradePortfolio from './PoweradePortfolio.jsx';
import CocaColaBottomSection from './CocaColaBottomSection.jsx';
import mountain from '../cocacola-images/powerade/powerademountain.png';
import lemon from '../cocacola-images/powerade/poweradelemon.png';
import passion from '../cocacola-images/powerade/poweradepassion.png';
import logopowerade from '../cocacola-images/logopowerade.png';
import bgLogo from '../cocacola-images/powerade/powerade-bg-logo.jpeg';
import background from '../cocacola-images/powerade/poweradebackground1.jpeg';

const poweradeDrinks = [lemon, mountain, passion];

const Powerade = ({ poweradeNutritionData }) => {

  return (
    <div>
      <div className={'powerade-logo-background'} style={{backgroundImage: `url(${bgLogo})`}}>
        <img src={logopowerade} alt={'Powerade logo'} />
      </div>
      <div className={'powerade-container'} style={{backgroundImage: `linear-gradient(90deg, ${poweradeNutritionData[0].color} 33.33%, ${poweradeNutritionData[1].color} 33.33%, ${poweradeNutritionData[1].color} 66.66%, ${poweradeNutritionData[2].color} 66.66%)`}}>
        <div className={'powerade-background'} style={{backgroundImage: `url(${background})`}}/>
        <div className={'powerade-drinks'}>
          {poweradeNutritionData.map((powerade) => (
            <a key={powerade.id} className="powerade-box-wrapper" href={() => false}>
              <div className={'powerade-drink-border justify-content-center'}>
                <div className={'powerade-drink'} style={{backgroundImage: `url(${poweradeDrinks[powerade.id]})`}}>
                  <div className={`powerade-box-content`}>
                    <PoweradeNutritionSlide nutrition={poweradeNutritionData[powerade.id]}/>
                  </div>
                </div>
              </div>
            </a>
          ))}
        </div>
      </div>
      <PoweradePortfolio bgLogo={bgLogo}/>
      <CocaColaBottomSection />
    </div>
  )
}

export default Powerade;