import React, { useState, useEffect } from 'react';
import { Routes, Route } from "react-router-dom";
import 'bootstrap/dist/css/bootstrap.css';
/*import styles from '../css/styles.module.css';*/
import '../css/BudatchDesign.css';
import Header from './Header';
import HeaderAssister from './HeaderAssister';
import Home from './Home';
import About from './About';
import Style from './Style';
import Latest from './Latest';
import Article from './Article';
import Contact from './Contact';
import Powerade from './Powerade';
import Cocacola from './Cocacola';
import Mer from './Mer';
import ProjectsCocacola from './ProjectsCocacola';
import ActorSearch from './ActorSearch';
import ActorPageSyncer from './ActorPageSyncer';
import Footer from './Footer';
import Error from './Error';
import ScrollToTop from './ScrollToTop';
import styleDataJson from '../data/stylesData.json';
import latestDataJson from '../data/latestData.json';
import aboutDataJson from '../data/aboutData.json';
import aboutQuotesDataJson from '../data/aboutQuotesData.json';
import cocacolaNutritionDataJson from '../data/coca-cola/cocacolaNutritionData.json';
import merNutritionDataJson from '../data/coca-cola/merNutritionData.json';
import poweradeNutritionDataJson from '../data/coca-cola/poweradeNutritionData.json';

const BudatchDesign = () => {
  const[projectId, setProjectId] = useState(0);
  const[styleData, setStyleData] = useState([]);
  const[latestData, setLatestData] = useState([]);
  const[aboutData, setAboutData] = useState([]);
  const[aboutQuotesData, setAboutQuotesData] = useState([]);
  const[merNutritionData, setMerNutritionData] = useState([]);
  const[cocacolaNutritionData, setCocaColaNutritionData] = useState([]);
  const[poweradeNutritionData, setPoweradeNutritionData] = useState([]);
  const[personIMDbData, setPersonIMDb] = useState(6193); //Leonardo Dicaprio TMDB Id number

  useEffect(() => {
    const loadData = () => {
      const loadedStyleData = [];
      const loadedLatestData = [];
      const loadedAboutData = [];
      const loadedAboutQuotesData = [];
      const loadedMerNutritionData = [];
      const loadedCocaColaNutritionData = [];
      const loadedPoweradeNutritionData = [];
  
      for(let data of styleDataJson) {
        loadedStyleData.push(
          data
        );
      }
  
      for(let data of latestDataJson) {
        loadedLatestData.push(
          data
        );
      }
  
      for(let data of aboutDataJson) {
        loadedAboutData.push(
          data
        );
      }
  
      for(let data of aboutQuotesDataJson) {
        loadedAboutQuotesData.push(
          data
        );
      }

      for(let data of merNutritionDataJson) {
        loadedMerNutritionData.push(
          data
        );
      }

      for(let data of cocacolaNutritionDataJson) {
        loadedCocaColaNutritionData.push(
          data
        );
      }

      for(let data of poweradeNutritionDataJson) {
        loadedPoweradeNutritionData.push(
          data
        );
      }
      setPoweradeNutritionData(loadedPoweradeNutritionData);
      setCocaColaNutritionData(loadedCocaColaNutritionData);
      setMerNutritionData(loadedMerNutritionData);
      setLatestData(loadedLatestData);
      setStyleData(loadedStyleData);
      setAboutData(loadedAboutData);
      setAboutQuotesData(loadedAboutQuotesData);
    }
    loadData();
  }, []);

  const _swapCompany = (id) => {
    setProjectId(id);
  }

  const _swapIMDbPerson = (personId) => {
    setPersonIMDb(personId);
  }

  //Without this "fake" loader the json file wont have the time to get fetched.
  if(styleData[projectId] === undefined) {
    return (
      <div>Loader</div>
    )
  }

  return (
    <div> 
      <Header />
      <ScrollToTop>
      <Routes>
        <Route path="/" element={
                        <Home 
                              id={projectId}
                              styleData={styleData}
                              swapCompany={_swapCompany}
                              latestData={latestData}
                              aboutQuotesData={aboutQuotesData}
                        />
        }/>
        <Route path="/about" element={
                        <About 
                              id={projectId}
                              aboutData={aboutData}
                              aboutQuotesData={aboutQuotesData}
                        />
        }/>
        <Route path="/style" element={
                          <Style  
                                id={projectId}
                                styleData={styleData}
                                swapCompany={_swapCompany}
                          />
        }/>
        <Route path="/latest" exact element={
                          <Latest 
                                latestData={latestData}
                          />
        }/>
        <Route path="/latest/:name" element={
                          <Article 
                                latestData={latestData}
                          /> 
                      }/>
        <Route path="/contact" element={
                          <Contact 
                          />
        }/>
        <Route path="/budatchprojects" exact element={
                        <HeaderAssister>
                          <ProjectsCocacola />
                          <div className={"d-flex"}>
                            <div className={"separator-line-reversed"}/>
                            <div className={"separator-line"}/>
                          </div>
                          <ActorSearch swapIMDbPerson={_swapIMDbPerson}
                          />
                        </HeaderAssister>
        }/>
        <Route path="/budatchprojects/coca-cola" element={
                        <HeaderAssister>
                          <Cocacola cocacolaNutritionData={cocacolaNutritionData}
                          />
                        </HeaderAssister>
        }/>
        <Route path="/budatchprojects/mer" element={
                        <HeaderAssister>
                          <Mer merNutritionData={merNutritionData}
                          />
                        </HeaderAssister>
        }/>
        <Route path="/budatchprojects/powerade" element={
                        <HeaderAssister>
                          <Powerade poweradeNutritionData={poweradeNutritionData}
                          />
                        </HeaderAssister>
        }/>
        <Route path="/budatchprojects/:name" element={
                        <HeaderAssister>
                          <ActorPageSyncer id={personIMDbData}
                          />
                        </HeaderAssister>
        }/>
        <Route path='*' element={<Error/>} />
      </Routes>
      </ScrollToTop>
      <Footer />
    </div>
  );
}

export default BudatchDesign;