import React, { useState, useEffect } from 'react';

import goldenglobe from '../images/goldenglobe.png';
import oscar from '../images/oscar.png';
import primetimeemmy from '../images/primetimeemmy.png';
import bafta from '../images/bafta.png';
import star from '../images/star.png';

import '../css/ActorAward.css';

const ActorAward = ({ awards }) => {
  const [award, setAward] = useState();
  const [height, setHeight] = useState("");
  const [fadeIn, setFadeIn] = useState(false);

  useEffect(() => {
    if(awards.toLowerCase().includes("oscar")) {
      setAward(oscar);
      setHeight("95px")
      setFadeIn(true);
    }
    if(awards.toLowerCase().includes("globe")) {
      setAward(goldenglobe);
      setHeight("120px")
      setFadeIn(true);
    }
    if(awards.toLowerCase().includes("primetime")) {
      setAward(primetimeemmy);
      setHeight("110px")
      setFadeIn(true);
    }
    if(awards.toLowerCase().includes("bafta")) {
      setAward(bafta);
      setHeight("100px")
      setFadeIn(true);
    }
    if(awards === undefined || awards === null || awards === "") {
      setAward(star);
      setHeight("90px")
    }
  }, [awards])

  return (
    <div style={{minHeight: "120px"}}>
      <img className={fadeIn ? ('fade-in') : (null)} src={award} alt={"award"} style={{height: height, marginRight: `${award === star ? ("1.8em") : ("3.5em")}`, marginBottom: `${award !== bafta ? (null) : ("15px")}`}}/>
    </div>
  )
}

export default ActorAward;