import React, { useState, useEffect } from 'react';
import MerFruitImage from './MerFruitImage.jsx';
import MerNutrition from './MerNutrition.jsx';
import MerYoutubePlayer from './MerYoutubePlayer.jsx';
import MerTetra from './MerTetra.jsx';
import CocaColaBottomSection from './CocaColaBottomSection.jsx';
import logomer from '../cocacola-images/logomer.png';
import background from '../cocacola-images/mer/mer-top-background.jpeg';
import apelsin from '../cocacola-images/mer/mer-apelsin.jpeg';
import paron from '../cocacola-images/mer/mer-paron.jpeg';
import apple from '../cocacola-images/mer/mer-apple.jpeg';
import hallonsvartvinbar from '../cocacola-images/mer/mer-hallon.jpeg';
import citrus from '../cocacola-images/mer/mer-citrus.png';
import kiwiapple from '../cocacola-images/mer/mer-kiwiapple.png';
import apelsin1 from '../cocacola-images/mer/fruits/orange1.png';
import apelsin3 from '../cocacola-images/mer/fruits/orange3.png';
import paron1 from '../cocacola-images/mer/fruits/pear1.png';
import paron2 from '../cocacola-images/mer/fruits/pear2.png';
import paron3 from '../cocacola-images/mer/fruits/pear3.png';
import rodapple1 from '../cocacola-images/mer/fruits/redapple1.png';
import rodapple2 from '../cocacola-images/mer/fruits/redapple2.png';
import rodapple3 from '../cocacola-images/mer/fruits/redapple3.png';
import hallon1 from '../cocacola-images/mer/fruits/raspberry1.png';
import svartvinbar2 from '../cocacola-images/mer/fruits/currant2.png';
import citrus2 from '../cocacola-images/mer/fruits/orange2.png';
import citrus3 from '../cocacola-images/mer/fruits/orange3.png';
import gronapple2 from '../cocacola-images/mer/fruits/greenapple2.png';
import kiwi1 from '../cocacola-images/mer/fruits/kiwi1.png';
import '../css/Mer.css';

const clipOneStyle = (left) => {
  return {
    clip: `rect(0, 100vw, auto, ${left}vw)`,
  }
}

const clipTwoStyle = (right) => {
  return {
    clip: `rect(0, ${right}vw, auto, 0vw)`,
  }
}

const juices = [apelsin, paron, apple, hallonsvartvinbar, citrus, kiwiapple];
const juiceFruits = [{apelsin3, apelsin1}, {paron1, paron2, paron3}, {rodapple1, rodapple2, rodapple3}, {svartvinbar2, hallon1}, {citrus2, citrus3}, {gronapple2, kiwi1}];

const Mer = ({ merNutritionData }) => {
  const [juiceId, setJuiceId] = useState(0); 
  const [xCoordinate, setXCoordinate] = useState(0);
  const [clipLeftOne, setClipLeftOne] = useState(0);
  const [clipRightTwo, setClipRightTwo] = useState(0);
  const [expanded, setExpanded] = useState(false);

  useEffect(() => {
    if(xCoordinate >= 5.7 && xCoordinate <= 20) {//apelsin
      setClipLeftOne('20');
      setClipRightTwo('0');
      setJuiceId(0);
    }
    if(xCoordinate >= 21 && xCoordinate <= 35) {//päron
      setClipLeftOne('35');
      setClipRightTwo('20');
      setJuiceId(1);
    }
    if(xCoordinate >= 36 && xCoordinate <= 50) {//äpple
      setClipLeftOne('51');
      setClipRightTwo('35');
      setJuiceId(2);
    }
    if(xCoordinate >= 51 && xCoordinate <= 65) {//hallon svartvinbär
      setClipLeftOne('65');
      setClipRightTwo('51');
      setJuiceId(3);
    }
    if(xCoordinate >= 66 && xCoordinate <= 80) {//citrusmix
      setClipLeftOne('80');
      setClipRightTwo('66');
      setJuiceId(4);
    }
    if(xCoordinate >= 81 && xCoordinate <= 94) {//kiwi äpple
      setClipLeftOne('95');
      setClipRightTwo('80');
      setJuiceId(5);
    }
    if(xCoordinate === 0 || xCoordinate < 5.7 || xCoordinate > 94) {
      setClipLeftOne('100');
      setClipRightTwo('0');
    }

  }, [xCoordinate]) 

  const handleOnMouseMove = (e) => {
    var xCoordinate = e.clientX;
    var xPercent = xCoordinate/window.innerWidth * 100;
    setXCoordinate(xPercent);
  }

  const _handleJuiceClick = (id) => {
    let newId = id;
    
    if(id < 0) {
      newId = merNutritionData.length - 1;
    }
    if(id >= merNutritionData.length) {
      newId = 0;
    }

    setTimeout(() => {
      setJuiceId(newId);
    }, 300);
  }

  const handleArrowColor = (id) => {
    let newId = id;

    if(id < 0) {
      newId = merNutritionData.length - 1;
    }
    if(id >= merNutritionData.length) {
      newId = 0;
    }

    return merNutritionData[newId].color;
  }

  return (
    <div>
      <div onMouseLeave={() => setXCoordinate(0)} onMouseMove={handleOnMouseMove}>
        <img className={'image-grey'} src={background} alt={'mer'} style={clipOneStyle(clipLeftOne)}/>
        <img className={'image-grey'} src={background} alt={'mer'} style={clipTwoStyle(clipRightTwo)}/>
        <img className={'image-coloured'} src={background} alt={'mer'}/>
      </div>
      <div className={'mer-title text-center'}>
        <img src={logomer} alt={'Mer logo'} style={{height: "1em"}}/>
        <p style={{color: `${merNutritionData[juiceId].color}`}}>– en svensk fruktdryck med unik smak</p>
      </div>
      <MerFruitImage juiceId={juiceId} merNutritionData={merNutritionData} juices={juices} juice={juices[juiceId]} juiceFruit={juiceFruits[juiceId]} handleJuiceClick={_handleJuiceClick}/>
      <div className={`nutrition ${expanded ? ('expanded') : (null)}`} onClick={() => setExpanded(!expanded)} style={{background: `linear-gradient(0.25turn, ${handleArrowColor(juiceId - 1)}, ${merNutritionData[juiceId].color}, ${handleArrowColor(juiceId + 1)})`}}>
        <strong><i className="fa fa-plus"></i> Visa Näringsinnehåll</strong>
        {expanded ? (
          <MerNutrition merNutritionData={merNutritionData[juiceId]}/>
        ) : (null)}
      </div>
      <MerTetra />
      <MerYoutubePlayer />
      <CocaColaBottomSection />
    </div>
  )
}

export default Mer;