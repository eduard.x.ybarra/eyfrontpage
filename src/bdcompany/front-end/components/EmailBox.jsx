import React, { useRef } from 'react';
import emailjs from '@emailjs/browser';

import '../css/EmailBox.css';

const EmailBox = ({ handlePopUp }) => {
  const form = useRef();

  const handleClick = () => {
    handlePopUp();
  }

  const handleOnSubmit = (e) => {
    e.preventDefault();

    emailjs.sendForm(
      'service_dlynk2d', 
      'template_99dbvaj', 
      form.current, 
      'EkttUeStV4RW1sZkh'
      )
      .then((result) => {
          console.log(result.text);
          console.log("Message sent!");
      }, (error) => {
          console.log(error.text);
      });
  }

  return (
    <div className={"popup"}>
      <div className={"popup-content"}>
        <span className="close" onClick={handleClick}>&times;</span>
        <div className={"row justify-content-center"}>
          <span onClick={handlePopUp} style={{marginBottom: "15px"}}>Email Budatch Design</span>
        </div>
        <form ref={form} onSubmit={handleOnSubmit}>
          <div className="form-row">
            <div className="form-group col-md-6">
              <label>First Name</label>
              <input type="text"required name="user_name" className="form-control" placeholder="Enter your First Name"/>
            </div>
            <div className="form-group col-md-6">
              <label>Last Name</label>
              <input type="text" required name="user_lastname" className="form-control" placeholder="Enter your Last Name" />
            </div>
          </div>
          <div className="form-row">
            <div className="form-group col-md-6">
              <label>Email Address</label>
              <input type="email" required name="user_email" className="form-control" placeholder="Enter your Email"/>
            </div>
            <div className="form-group col-md-6">
              <label>Phone (Optional)</label>
              <input type="name" name="user_phone" className="form-control" placeholder="Enter your Phone Number" />
            </div>
          </div>
          <div className="form-group">
            <label>Address (Optional) </label>
            <input type="text" name="user_adress" className="form-control" placeholder="Enter your Adress"/>
          </div>
          <div className="form-group">
            <label>Comment:</label>
            <textarea required type="message" name="message" className="form-control" rows="5"></textarea>
          </div>
          <input className={"popup-submit-button"} type="submit" value="Submit" />
        </form>
      </div>
    </div>
  )
}

export default EmailBox;