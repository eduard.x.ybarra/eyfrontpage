import React from 'react';
import { useNavigate } from 'react-router-dom';
import '../css/CocaColaBottomSection.css';
import logopowerade from '../cocacola-images/logopowerade.png';
import logococacola from '../cocacola-images/logococacola.png';
import logomer from '../cocacola-images/logomer.png';
import cocacola from '../cocacola-images/cc-bottom/bottom-cocacola.png';
import powerade from '../cocacola-images/cc-bottom/bottom-powerade.png';
import mer from '../cocacola-images/cc-bottom/bottom-mer.png';
import royal from '../cocacola-images/cc-bottom/royal-in-works.png';
import fanta from '../cocacola-images/cc-bottom/fanta-in-works.png';
import sprite from '../cocacola-images/cc-bottom/sprite-in-works.png';
import bonaqua from '../cocacola-images/cc-bottom/bonaqua-in-works.png';

const reworkedBrands = [[cocacola, logococacola, "coca-cola"], [powerade, logopowerade, "powerade"], [mer, logomer, "mer"]];

const CocaColaBottomSection = () => {

  const navigate = useNavigate();

  const handleOnClick = (path) => {
    console.log(path)
    window.scrollTo(0, 0);
    navigate("/budatchdesign/budatchprojects/" + path);
  }

  return (
    <div className={'cc-bottom-container'}>
      <h2>Completed brand reworks by Budatch Design</h2>
      <div className={'cc-card-container'}>
      {reworkedBrands.map((brand, i) => (
        <div key={i} className={'cc-reworked-card'} onClick={() => handleOnClick(brand[2])}>
          <img className={'cc-top'} src={brand[0]} alt={'logo-representation'}/>
          <div className={'card-seperator'} />
          <img className={'cc-bot'} src={brand[1]} alt={'brand-representation'}/>
        </div>
      ))}
      </div>
      <h3>More to come</h3>
      <div className={'cc-in-works'}>
        <img src={fanta} alt={'brand-in-works'}/>
        <img src={sprite} alt={'brand-in-works'}/>
        <img src={royal} alt={'brand-in-works'}/>
        <img src={bonaqua} alt={'brand-in-works'}/>
      </div>
    </div>
  )
}

export default CocaColaBottomSection;