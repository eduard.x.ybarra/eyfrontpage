import React, { useState, useEffect } from 'react';

import '../css/HeaderVideo.css';

const HeaderVideo = ({ video, path }) => {
  const[scroll, setScroll] = useState(window.pageYOffset);
  const[blur, setBlur] = useState(200);

  useEffect(() => {
    window.addEventListener("scroll", handleScroll);

    if(scroll === 0) {
      setBlur(80);
    } else {
      setBlur(0)
    }

    return () => window.removeEventListener("scroll", handleScroll);
  }, [scroll])

  const handleScroll = () => {
    setScroll(window.pageYOffset);
  }

  return (
    <div className={"header-video"}>
      {scroll < 270 ? (
        <div>
          <video src={video} type="video/mp4" muted={true} autoPlay loop style={{filter: `blur(${blur}px)`}}/>
          <p className={scroll === 0 ? ('header-video-text-is-loaded') : ('header-video-text')} style={{opacity: scroll === 0 ? ("1") : ("0")}}>{path}</p>
        </div>
      ) : (null)}
    </div>
  )
}

export default HeaderVideo;