import React, { useState, useEffect } from 'react';
import logoIMDB from '../images/imdblogo.svg';
import logoTMDB from '../images/tmdblogo.png';
import '../css/ActorBiography.css';

const ActorBiography = ({ biography, biographyIMDB }) => {
	const [chosenBiography, setChosenBiography] = useState(biography);
	const [noBiography, setNoBiography] = useState(false);
	const [limitedParagraph, setLimitedParahraph] = useState(true);
	const [loadingIMDBBiography, setLoadingIMDBBiography] = useState(true);

	useEffect(() => {
		if(biography === "") {
			setChosenBiography(biographyIMDB);
			setNoBiography(true);
		}
		if(biographyIMDB !== "") {
			setLoadingIMDBBiography(false);
		}
	}, [biography, biographyIMDB])

	const handleBiography = (biography) => {
		setChosenBiography(biography);
  }

	const handleOnClick = () => {
		setLimitedParahraph(!limitedParagraph)
	}

  return (
		<div>
			<div className={'poweredby-biographies'}>
				<img className={'img-responsive'} src={logoTMDB} alt="Budatch Design" onClick={noBiography ? (null) : (() => handleBiography(biography))} style={{filter: noBiography ? ("grayscale(100%)") : (null), cursor: noBiography ? (null) : ("pointer")}}/>
				<div className={'image-spinner'}>
					<img className={'img-responsive'} src={logoIMDB} alt="Budatch Design" onClick={loadingIMDBBiography ? (null) : (() => handleBiography(biographyIMDB))} style={{filter: loadingIMDBBiography ? ("grayscale(100%)") : (null), cursor: loadingIMDBBiography ? (null) : ("pointer")}}></img>
					{loadingIMDBBiography ? (
						<span className={'spinner-border spinner-border-sm'} role="status" aria-hidden="true"></span>
					) : (null)}
				</div>
			</div>
			<div className={'actor-biography'} style={{border: `solid 0.8em ${chosenBiography === biography ? ("#032541") : ("#F5C518")}`}}>
				<div className={limitedParagraph ? ('biography-text-limited') : ("biography-text")}>
					<p>{chosenBiography}</p>
				</div>
				{limitedParagraph ? (
					<div className={'read-more-bio-content'}>
						<span onClick={() => handleOnClick()}>Read More</span>
						<i className="fa fa-angle-down fa-sm" style={{marginTop: "-4px"}}></i>
					</div>
					) : (
					<div className={'read-more-bio-content'}>
						<i className="fa fa-angle-up fa-sm" style={{marginBottom: "-4px"}}></i>
						<span onClick={() => handleOnClick()}>Read Less</span>
					</div>
				)}
			</div>
		</div>
	)
}

export default ActorBiography;