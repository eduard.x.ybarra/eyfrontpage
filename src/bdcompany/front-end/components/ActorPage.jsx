import React, { useState, useEffect } from 'react';

import useIMDBService from './FetchData/useIMDBService.jsx';
import MediaCarousel from './MediaCarousel.jsx';
import ActorInformation from './ActorInformation.jsx';
import CreditSection from './CreditSection.jsx';
import ActorBiography from './ActorBiography.jsx';

import '../css/ActorPage.css';

const imageURL = "https://image.tmdb.org/t/p/w342";

const ActorPage = ({ externalIds, movieCredits, tvCredits, combinedCredits, crew, department, birthplace, birthday, biography, images, name }) => {
  const [{awards, biographyIMDB, trivias, knownFor, knownForImages, jobs}, setIMDBUrl] = useIMDBService();
  const [titles, setTitles] = useState([]);
  const [posters, setPosters] = useState([]);
  const [relevantMovies, setRelavantMovies] = useState([]);
  const [profileImages, setProfileImages] = useState([]);
  const [hideCarousel, setHideCarousel] = useState(true);
  
  useEffect(() => {
    const relevantMovies = [];
    const randomImages = [];
    const tmdbPosters = [];
    const tmdbTitles = [];
    
    for(const movie of movieCredits) {
      if(movie.poster_path !== null && movie.character !== null) {
        if(movie.character !== undefined) {
          if(!movie.character.toLowerCase().includes("himself")) {
            if(movie.popularity >= 1) {
              relevantMovies.push({ name: movie.title, poster: movie.poster_path, id: movie.credit_id })
            }
          }
        }  
      }
    }

    for(const poster of images) {
      randomImages.push(poster.file_path);
    }

    const shortRelevantMovies = relevantMovies.slice(0, 4);

    for(const movie of shortRelevantMovies) {
      tmdbPosters.push(imageURL + movie.poster);
    }

    for(const movie of shortRelevantMovies) {
      tmdbTitles.push(movie.name);
    }

    setTitles(tmdbTitles);
    setPosters(tmdbPosters);
    setIMDBUrl("https://www.imdb.com/name/" + externalIds.imdb_id);
    setRelavantMovies(relevantMovies);
    setProfileImages(randomImages);
  }, [images, externalIds, movieCredits, setIMDBUrl]);

  const handleOnClick = () => {
    setHideCarousel(!hideCarousel)
  }

  return (
    <div>
      {profileImages.length ? (
        <div>
          <MediaCarousel movies={relevantMovies} hideCarousel={hideCarousel}/>
          <div className={'media-carousel-showmore'}>
          {hideCarousel ? (
            <div className={'show-mediacarousel'}>
              <span onClick={() => handleOnClick()}>Hide Media Carousel</span>
              <i className="fa fa-angle-up fa-sm" style={{marginBottom: "-4px"}}></i>
            </div>
            ) : (
            <div className={'show-mediacarousel'}>
              <span onClick={() => handleOnClick()}>Show Media Carousel</span>
              <i className="fa fa-angle-down fa-sm" style={{marginTop: "-4px"}}></i>
            </div>
				)}
          </div>
          <div className={'actor-page-container'}>
            <ActorInformation externalIds={externalIds} 
                              name={name}
                              profileImages={profileImages}
                              titles={titles}
                              posters={posters}
                              department={department} 
                              birthday={birthday} 
                              birthplace={birthplace}
                              awards={awards} 
                              trivias={trivias} 
                              knownFor={knownFor} 
                              knownForImages={knownForImages}
                              jobs={jobs}
                              />
            <ActorBiography biography={biography} biographyIMDB={biographyIMDB}/>
            <CreditSection movies={movieCredits} tv={tvCredits} combined={combinedCredits} crew={crew}/>
          </div>
          <MediaCarousel movies={relevantMovies} hideCarousel={hideCarousel}/>
        </div>
      ) : (
        <div>
          Loading...
        </div>
      )}
    </div>
  )
}

export default ActorPage;