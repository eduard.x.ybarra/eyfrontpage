import React from 'react';
import { Routes, Route } from 'react-router-dom';
import FrontPage from './eyfrontpage/FrontPage.jsx';
import BudatchDesign from './bdcompany/front-end/components/BudatchDesign.jsx';
import FirstPortfolio from './firstportfolio/portfolio/sections/Portfolio.jsx';
import RingBling from './firstportfolio/ringbling/Game/RingBling.jsx'
import Calendar from './firstportfolio/calendar/InputCalendar.jsx'
import Curriculum from './firstportfolio/curriculum/Curriculum.jsx'
import SecondPortfolio from './secondportfolio/hookcomponents/SecondPortfolio.jsx';
import './App.css';

const App = () => {

  return (
    <div className="App">
      <Routes>
        <Route path="/" exact element={<FrontPage />} />
        <Route path="/budatchdesign/*" element={<BudatchDesign />} />
        <Route path="/firstportfolio" element={<FirstPortfolio />} />
        <Route path="/ringbling" element={<RingBling boardSize={13} playerSize={25}/>} />
        <Route path="/calendar" element={<Calendar />} />
        <Route path="/curriculum" element={<Curriculum />} />
        <Route path="/secondportfolio" element={<SecondPortfolio />} />
      </Routes>
    </div>
  )
}

export default App;
