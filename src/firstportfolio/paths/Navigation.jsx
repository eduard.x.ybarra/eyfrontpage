import React from 'react';
import { NavLink } from 'react-router-dom';


const Navigation = () => {
  return(
    <div>
      <NavLink style={{padding: '5px'}} to="/">Home</NavLink>
      {/* <NavLink to="/aboutme">AboutMe</NavLink>
      <NavLink to="/contact">Contact</NavLink> */}
      <NavLink to="/calendar">Calendar</NavLink>
      <NavLink to="/curriculum">Curriculum</NavLink>
      <NavLink to="/ringbling">Ringbling</NavLink>
      <NavLink to="/portfolio">Portfolio</NavLink>
    </div>
  )
}

export default Navigation;