import React from 'react';
import image from './blackedison.jpg';
import './Curriculum.css';

const Curriculum = () => {
  return (
    <div className="wrapper" id="cv">
      <div className="mainDetails">
          <div className="profilePicture" id="headshot">
              <img src={image} alt="Eduard Ybarra" />
          </div>
          <div id="name">
              <h1 className="nameInfo">Eduard Ybarra</h1>
          </div>
          <div className="contactInfo" id="contactDetails">
          <p>
          1994-11-12 <br/>Södra Jordbrovägen 167 <br/>Jordbro 137 62<br/>
          </p>
          <div className="ullist">
            <ul>
              <li>mail: <a href="new_edward@hotmail.com">eduard.x.ybarra@gmail.com</a></li>
              <li>mob: 070-2266077</li>
              <li>github: <a href="https://github.com/Tsiones">github.com/Tsiones</a></li>
              <li>gitlab: <a href="https://gitlab.com/eduard.x.ybarra">gitlab.com/Eduard.x.ybarra</a></li>
            </ul>
          </div>
          <div className="clear"></div>
      </div>
      </div>
      <div className="mainWrapper" id="mainArea">
          <div className="section">
                  <div className="sectionTitle">
                      <h1>Sammanfattning</h1>
                  </div>
                  <div className="sectionContent">
            <p>
                              Jag ser mig själv som en social person som gillar att arbeta med människor då den ena situationen inte är den andre lik.
                              Mina stöttepelare i mitt arbete är nyfikenhet och engagemang. Jag går in i mitt arbete helhjärtat och har en hög arbetsmoral.
                              Jag gillar att ha många bollar i luften och är alltid villig att lära mig något nytt.
            </p>
                  </div>
              <div className="clear"></div>
          </div>

          <div className="section">
              <div className="sectionTitle">
                  <h1>Utbildning</h1>
              </div>
              <div className="sectionContent">
                      <div className="subsection">
                      <h2>YH C3L: Java Systemutveckling</h2>
                      <p className="subDetails">2015-2017</p>
                      <p> Java, Spring Data-JPA, JavaScript, React, Redux, Node-js, HTML &amp; CSS, mySQL och Andriod Studio
                          </p>
                  </div>
                  <div className="subsection">
                      <h2>Fredrika Bremergymnasiet</h2>
                      <p className="subDetails">2010-2013</p>
                      <p></p>
                  </div>
              </div>
              <div className="clear"></div>
          </div>

              <div className="section">
              <div className="sectionTitle">
                  <h1>Erfarenhet</h1>
              </div>
              <div className="sectionContent">
                      <div className="subsection">
                          <h2>Coca-Cola Enterprises Sverige AB</h2>
                          <p className="subDetails">2014 - 2015</p>
                          <p>Beredningen - Blanda produkter</p>
                      </div>
              </div>
              <div className="clear"></div>
          </div>

      <div className="section">
        <div className="sectionTitle">
          <h1>Kunskaper</h1>
        </div>

        <div className="sectionContent">
          <div className="subsection">
            <h2>Språk</h2>
            <p className="subDetails"></p>
            <p>Svenska, modersmål<br/>
              Engelska, goda kunskaper i tal och skrift<br/>
                          Ryska, goda kunskaper i tal</p>
          </div>

          <div className="subsection">
            <h2>IT</h2>
            <p className="subDetails"></p>
            <p>
              Övrigt: Van att arbeta med Windows och Mac
            </p>
          </div>
        </div>
        <div className="clear"></div>
        </div>
    </div>
  </div>
  )
}

export default Curriculum;