import React, { useState } from 'react';
import Calendar from './Calendar.jsx';
import Text from './Text.jsx';

const InputCalendar = () => {

  const [savedDates, setSavedDates] = useState([])

  const handleDateText = (date) => {
    let exists = false;
    for (let i = 0; i < savedDates.length; i++) {
      if (savedDates[i].date === date.date) {
        exists = true;
      } 
    }
    if(!exists) {
      savedDates.push(date);
      const uniqueDates = Array.from(new Set(savedDates));
      setSavedDates(uniqueDates);
    } else {
      const showSavedDate = [...savedDates];
      for(let i = 0; i < savedDates.length; i++) {
        if(showSavedDate[i].date === date.date) {
          showSavedDate[i].showDate = true;
          setSavedDates(showSavedDate);
        }
      }
    }
  }
  
  const _showDate = (currentDate, hideDate) => {
    const newSavedDate = [...savedDates];
    for(let i = 0; i < savedDates.length; i++) {
      if(newSavedDate[i].date === currentDate) {
        newSavedDate[i].showDate = hideDate;
        setSavedDates(newSavedDate);
      }
    }
  }

  const _dateTextSubmit = (dateName, currentDate) => {
    setSavedDates(savedDates.map(date => (date.date === dateName ? currentDate : date)))
  }

  return (
    <div className="App">
      <Calendar handleDateText={handleDateText} />
      {savedDates.map((dates) => (
        <Text key={dates.date} dates={dates} showDate={_showDate} dateTextSubmit={_dateTextSubmit}/>
      ))}
    </div>
  )
}

export default InputCalendar;