import React, { useState, useEffect, useRef } from 'react';
import Board  from '../GameObjects/Board.jsx';
import Player  from '../GameObjects/Player.jsx';
import Rings from '../GameObjects/Rings.jsx';
import Scoreboard  from '../GameObjects/Scoreboard.jsx';
import { rngColor } from '../Constants/randomgenerator.js';
import { rngNumber } from '../Constants/randomgenerator.js';

function useInterval(callback, delay) {
  const savedCallback = useRef();

  useEffect(() => {
    savedCallback.current = callback;
  }, [callback]);

  useEffect(() => {
    function tick() {
      savedCallback.current();
    }
    if (delay !== null) {
      let id = setInterval(tick, delay);
      return () => clearInterval(id);
    }
  }, [delay]);
}

const getGameState = ({ boardSize, playerSize }) => {
  const half = Math.floor(boardSize / 2) * playerSize;
  return {
      size: {
          board: boardSize,
          player: playerSize,
          maxDim: boardSize * playerSize
      },
      player: {
          top: half,
          left: playerSize
      },
      score: 0,
      highScore: 0,
      totalRings: 0
  }
};

const RingBling = ({boardSize, playerSize}) => {

  const[gameState, setGameState] = useState(getGameState({boardSize, playerSize}));
  const[color, setColor] = useState(rngColor(['Blue', 'Green', 'Red', 'Yellow']));
  const[rings, setRings] = useState([]);
  const[gameOver, setGameOver] = useState(false);

  useEffect(() => {
    window.onkeydown = handleKeys;
  })

  const handleKeys = (event) => {
    if(gameOver === false) {
      let newRings;

      switch(event.keyCode) {
        case 87:
        case 38: {
          newRings = rings.map((ring) => {
            ring.top = (ring.top + playerSize);
            if(ring.top === boardSize * playerSize) {
              ring.top = 0;
            }
            return ring;
          })
          setRings(newRings);
          break;
        }
        case 83: 
        case 40: {
          newRings = rings.map((ring) => {
            ring.top = (ring.top - playerSize);
            if(ring.top === -playerSize) {
              ring.top = (boardSize * playerSize - playerSize);
            }
            return ring;
          })
          setRings(newRings);
          break;
        }
        default:
          return;
      }
    }
  }
  
  const updateRings = () => {
    let newColor = color;
    const newRings = [];

    rings.forEach((ring) => {
      const newRing = { ...ring };
      newRing.left = ring.left - (playerSize / ring.speed);

      if (ring.left <= -playerSize) {
        return;
      }
      else if (ring.top === gameState.player.top) {
        if (ring.left >= (playerSize - 20) && ring.left <= playerSize * 2.5) {
          if(ring.color === color) {
            gameState.score = gameState.score + 1;
            newColor = rngColor(['Blue', 'Green', 'Red', 'Yellow'].filter(c => !color.includes(c)));
            return;
          } else {
            setGameOver(true);
            return;
          }
        }
      }
      newRings.push(newRing);
    });
    if(gameOver === true) {
      setRings([]);
    } else {
      setRings([...newRings]);
    }
    setColor(newColor);
  };

  const spawnRings = () => {
    let newRing = {};
    let { totalRings } = gameState;
    let generatedTop = rngNumber(0, boardSize) * playerSize;
    
    setGameState({...gameState, totalRings: totalRings + 1})
    
    for(let ring of rings) {
      if(ring.top === generatedTop) {
        generatedTop = rngNumber(0, boardSize) * playerSize;
      }
      for(let ring of rings) {
        if(ring.top === generatedTop) {
          generatedTop = rngNumber(0, boardSize) * playerSize;
        }
      }
    }

    newRing.key = totalRings + 1;
    newRing.left = (boardSize * playerSize);
    newRing.top = generatedTop;
    newRing.speed = rngNumber(3, 10);
    newRing.color = rngColor(['Blue', 'Green', 'Red', 'Yellow']);

    setRings([...rings].concat(newRing));
  }

  useInterval(updateRings, 50);
  useInterval(spawnRings, 450); //spawn rings 500 - 800

  const handleGameOver = (score) => {
    if(score > gameState.highScore) {
      gameState.highScore = score;
    }
    gameState.score = 0;
    setGameOver(false);
  }

  return (
    <div style={{height: "100vh", textAlign: "center", background: "black", paddingBottom: "20px"}}>
      <div style={{textAlign: "center", paddingTop: "20px"}}>
        <p style={{color: "white"}}>Press W or Up-arrow key to move your hero up!</p>
        <p style={{color: "white"}}>Press S or Down-arrow key to move your hero down!</p>
        <p style={{color: "white"}}>Highscore: {gameState.highScore}</p>
        <p style={{color: "white"}}>Score: {gameState.score}</p>
      </div>
      {gameOver ? (
        <div>
          <Board color={color} dimension={gameState.size.board * gameState.size.player}>
            <Scoreboard score={gameState.score} highScore={gameState.highScore} handleGameOver={handleGameOver}/>
          </Board>
        </div>
      ) : (
        <div>
          <Board dimension={gameState.size.board * gameState.size.player} color={color} >
            <Player size={gameState.size.player} top={gameState.player.top} left={gameState.player.left} color={color} />
            <Rings rings={rings} 
                   size={gameState.size.player} 
                   boardSize={boardSize}
            />
          </Board>
        </div>
      )}
    </div>
  )
}

export default RingBling;