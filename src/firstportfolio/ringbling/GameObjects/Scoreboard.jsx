import React from 'react';
import './Scoreboard.css';

const Scoreboard = ({ score, highScore, handleGameOver }) => {
  
  const handleClick = () => {
    handleGameOver(score);
  }

  return (
    <div style={{color: 'white'}} className='scoreboard-center'>
      <h1 style={{color: 'grey'}}>GAME OVER</h1>
      <h2 style={{color: 'grey'}}>Highscore: {highScore > score ? (highScore) : (score)}</h2>
      <h3 style={{color: 'grey'}}>Score: {score}</h3>
      <h1 onClick={handleClick} style={{cursor: "pointer"}}>CLICK HERE TO PLAY AGAIN!</h1>
    </div>
  )
}

export default Scoreboard;