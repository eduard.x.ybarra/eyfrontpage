import React, { useState } from 'react';
import Menu from './Menu.jsx';
import Description from './Description.jsx';
import Projects from './Projects.jsx';
import Contact from './Contact.jsx';
import Information from './Information.jsx';
import Translate from "i18n-react-json";
import locales from "../locales";
import '../css/Portfolio.css';

const Portfolio = () => {
  const [language, setLangauge] = useState('en')
  const T = new Translate(locales);

  T.setLocale(language);

  const changeLanguage = (language) => {
    setLangauge(language);
  }

  return (
    <div className="Portfolio">
      <Menu changeLanguage={changeLanguage}
            home={T.__("HOME")}
            projects={T.__("PROJECTS")}
            aboutMe={T.__("ABOUT ME")}
            contact={T.__("GET IN TOUCH")}
      />
      <Description description={T.__("Description")}
                   myProjects={T.__("My projects")}
                   aboutMe={T.__("About Me")}
      />
      <Projects project={T.__("Projects")}
                project1={T.__("Name of project1")} 
                project2={T.__("Name of project2")} 
                project3={T.__("Name of project3")}
                project4={T.__("Name of project4")}
                project5={T.__("Name of project5")}
                seeProject={T.__("See Project")}
      />
      <Contact contact={T.__("Contact")}
               sendEmail={T.__("SendEmail")}
      />
      <Information title={T.__("Title")}/>
    </div>
  );
}

export default Portfolio;