import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faEnvelope } from '@fortawesome/free-solid-svg-icons'
import { faLinkedinIn, faGithub, faGitlab } from "@fortawesome/free-brands-svg-icons"
import '../css/Information.css';

const Information = ({ title }) => {

  return (
    <div className="information">
      <div className="left">
        <h3>Eduard Ybarra</h3>
        <p>{title}</p>
        <ul className="list">
          <li>Södra Jordbrovägen 167</li>
          <li>Jordbro 137 62</li>
          <li>SE - Sweden</li>
        </ul>
      </div>
      <div className="right">
        <div className="social-links">
          <a href={`mailto:eduard.x.ybarra@gmail.com`} rel="noopener noreferrer" target="_blank">
            <FontAwesomeIcon icon={faEnvelope} />
          </a>
          <a href="https://www.linkedin.com/in/eduard-ybarra-a49459137" rel="noopener noreferrer" target="_blank">
            <FontAwesomeIcon icon={faLinkedinIn} />
          </a>
          <a href="https://github.com/Tsiones" rel="noopener noreferrer" target="_blank">
            <FontAwesomeIcon icon={faGithub} />
          </a>
          <a href="https://gitlab.com/eduard.x.ybarra" rel="noopener noreferrer" target="_blank">
            <FontAwesomeIcon icon={faGitlab} />
          </a>
        </div>
        <ul className="list">
          <li>eduard.x.ybarra@gmail.com</li>
          <br/>
          <li>+46 70-2266077</li>
        </ul>
      </div>
    </div>
  )
}

export default Information;