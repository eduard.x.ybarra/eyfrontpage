import React from 'react';
import Eddyted from '../image/blackedison.jpg'
import '../css/Description.css';

const Description = ({ description, myProjects, aboutMe }) => {

  const scrollProjects = () => {
    window.scrollTo(0, 390);
  }

  return (
    <div className="description-container">
      <div className="image">
        <img src={Eddyted} alt="Eduard Ybarra"/>
      </div>
      <div className="description-content">
        <h3>{aboutMe}</h3>
        <p>{description}</p>
        <button onClick={scrollProjects}>{myProjects}</button>
      </div>
    </div>
  )
}

export default Description;