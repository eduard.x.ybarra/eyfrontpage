import React, { useState } from 'react';
import { Link } from "react-router-dom";
import '../css/Menu.css';

const Menu = ({ changeLanguage, home, projects, contact }) => {
  const [englishLang, setEnglighLang] = useState(true);

  const getInTouch = () => {
    window.scrollTo(0, 700);
  }

  const scrollProjects = () => {
    window.scrollTo(0, 260);
  }

  const onClickSE = () => {
    changeLanguage('se')
    setEnglighLang(false);
  }

  const onClickEN = () => {
    changeLanguage('en')
    setEnglighLang(true);
  }

  const style = () => {
    return {
      textDecoration: 'underline'
    }
  }

  return (
    <div className="menu">
      <div className="langauges">
        <p onClick={onClickSE} style={englishLang ? null : style()}>Sv</p>
        <p>|</p>
        <p onClick={onClickEN} style={englishLang ? style() : null}>En</p>
      </div>
      <div className="name">
        <h2>Eduard Ybarra</h2>
      </div>
      <div className="routes">
        <p onClick={getInTouch}>{contact}</p>
        <p onClick={scrollProjects}>{projects}</p>
        <Link to="/">
          <p>{home}</p>
        </Link>
      </div>
    </div>
  )
}

export default Menu;