import React from 'react';
import { useState, useEffect } from 'react';
import education from '../images/education.png';
import experience from '../images/experience.png';
import skills from '../images/skills.png';
import SwitchComponents from './SwitchComponents';
import Skills from './Skills.jsx';
import Education from './Education.jsx';
import Experience from './Experience.jsx';
import '../css/About.css';

const About = ({ aboutData }) => {
  const[index, setIndex] = useState(1)
  const[activeComponent, setActiveCompoenent] = useState(aboutData[index].name)

  const nextPersonalInformation = (direction) => {

    var newIndex;

    if(direction < 0) {
      if(index + direction < 0) {
        newIndex = aboutData.length - 1;
      } else {
        newIndex = index - 1;
      }
    }

    if(direction > 0) {
      if(index + direction > aboutData.length - 1) {
        newIndex = 0;
      } else {
        newIndex = index + 1;
      }
    }

    setIndex(newIndex);
    setActiveCompoenent(aboutData[newIndex].name);
  }

  const nextPersonalPick = (index) => {
    setIndex(index);
    setActiveCompoenent(aboutData[index].name);
  }

  return (
    <div className={"about-section"}>
      <div className={"triangle"}>
        <div></div>
        <div></div>
        <div></div>
        <div></div>
        <div></div>
        <div></div> 
      </div>
      <h2>About Me</h2>
      <div className={"about-information"}>
        <div>
          <p>Active, dedicated and young professional with a burning passion for technology looking for a career that fits my skills, personality and ambition. Challenges, especially problem solving, are among the things I enjoy the most!</p>
        </div>
      </div>
      <div className={"about-information-personal"}>
        <div className={"arrow-capsule-left"}>
          <i className={"about-arrow-left"} onClick={() => nextPersonalInformation(-1)}></i>
        </div>
        {Object.values(aboutData).map((info, i) => (
          <p className={`${index === i ? ("personal-active") : ("personal-choice")}`} key={i} onClick={() => nextPersonalPick(i)} style={{marginRight: i === 1 ? (`10px`) : (""), marginLeft: i === 1 ? (`10px`) : ("") }}>{info.name}</p>
        ))}
        <div className={"arrow-capsule-right"}>
          <i className={"about-arrow-right"} onClick={() => nextPersonalInformation(1)}></i>
        </div>
      </div>
      <div className={"about-information-extra"}>
        <SwitchComponents active={activeComponent}>
          <Education name="Education"/>
          <Skills name="Skills"/>
          <Experience name="Experience"/>
        </SwitchComponents>
        <div className={"about-images"}>
          <img className={"about-experience-image"} src={experience} alt="experience" style={{transform: activeComponent === "Experience" ? ("translate(-532px, -126px)") : ("translate(-500px, -110px)"), filter: activeComponent === "Experience" ? ("") : ("grayscale(100%)")}}/>
          <img className={"about-education-image"} src={education} alt="education" style={{transform: activeComponent === "Education" ? ("translate(-420px, -238px)") : ("translate(-372px, -190px)"), filter: activeComponent === "Education" ? ("") : ("grayscale(100%)")}}/>
          <img className={"about-skills-image"} src={skills} alt="skills" style={{transform: activeComponent === "Skills" ? ("translate(-340px, -14px)") : ("translate(-388px, -102px)"), filter: activeComponent === "Skills" ? ("") : ("grayscale(100%)")}}/>
          <img className={"about-experience-image"} src={experience} alt="experience" style={{zIndex: "3", transform: activeComponent === "Experience" ? ("translate(-532px, -126px)") : ("translate(-500px, -110px)"), opacity: activeComponent === "Experience" ? ("1") : ("0")}}/>
          <img className={"about-education-image"} src={education} alt="education" style={{zIndex: "3", transform: activeComponent === "Education" ? ("translate(-420px, -238px)") : ("translate(-372px, -190px)"), opacity: activeComponent === "Education" ? ("1") : ("0")}}/>
          <img className={"about-skills-image"} src={skills} alt="skills" style={{zIndex: "3", transform: activeComponent === "Skills" ? ("translate(-340px, -14px)") : ("translate(-388px, -102px)"), opacity: activeComponent === "Skills" ? ("1") : ("0")}}/>
        </div>
      </div>
    </div>
  )
}

export default About;