import React from "react";
import Tilt from 'react-parallax-tilt';
import edison from '../images/blackedison.jpg';
import suit from '../images/lightweight.png';
import point from '../images/pointercss.png';
import '../css/Home.css';

const Home = () => {
    return (
      <div className={"home-section"}>
        <div className="circle">
          <div></div>
          <div></div>
          <div></div>
          <div></div>
          <div></div>
          <div></div>
        </div>
        <div className={"portrait-main"}>
          <div className={"portrait-combiner"}>
            <figure>
              <img className={"portrait-edison"} src={edison} alt="" ></img>
            </figure>
            <img className={"portrait-suit"} src={suit} alt="" ></img>
            <img className={"portrait-pointer"} src={point} alt="" ></img>
          </div>
          <Tilt transitionSpeed={5000} tiltEnable={false} glareEnable={true} glareMaxOpacity={0.3} glareColor="white" glareBorderRadius="20px" glarePosition='all' className={"portrait-description"}>
            <strong>Hello, my name is</strong>
            <h2>Eduard Ybarra</h2>
            <p>I am a future Web Developer</p>
          </Tilt>
        </div>
      </div>
    )
}

export default Home;