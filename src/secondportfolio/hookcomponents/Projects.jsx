import React, { useState, useEffect } from 'react';
import ProjectLocker from './ProjectLocker.jsx';
import ImageGallary from './ImageGallary.jsx';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faArrowRight } from "@fortawesome/free-solid-svg-icons";
import '../css/Projects.css';

const Projects = ({ projectsData }) => {
  const[selectedProject, setSelectedProject] = useState(null);

  const activeSelectedProject = (projectNumber) => {
    if(projectNumber === selectedProject) {
      setSelectedProject(null)
    } else {
      setSelectedProject(projectNumber)
    }
  }

	return (
    <div className={"projects-section"}>
      <div className={"box"}>
        <div></div>
        <div></div>
        <div></div>
        <div></div>
        <div></div>
        <div></div>
      </div>
      <h2>Projects</h2>
      <div className={"project-locker-container"}>
      {projectsData.map((data) => (
        <ProjectLocker key={data.id} projectNumber={data.id} selectedProject={selectedProject} activeSelectedProject={activeSelectedProject}/>
      ))}
      </div>
      <div className={"projects-container"}>
      {projectsData.map((data) => (
        <div  key={data.id} 
              className={`${selectedProject === data.id ? ("project-item") : ("project-item collapse")}`}
              style={{boxShadow: selectedProject === data.id ? ("0px 0px 20px 5px green") : ("")}} 
              onClick={() => activeSelectedProject(data.id)}
        >
        {selectedProject === data.id ? (
          <div className={"project-information"}>
            <p>{data.description}</p>
            <ImageGallary photos={data.photos}/>
          </div>
          ) : (
          <div className={"project-introduction"}>
            <p>{data.name}</p>
            <p className={"introduction-view"}>
            Click To View Project
            </p>
            <FontAwesomeIcon icon={faArrowRight} size="3x"/>
          </div>
        )}
        </div>
      ))}
      </div>
    </div>
  )
}

export default Projects;