import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faHouse, faDiagramProject, faAddressCard, faAddressBook } from "@fortawesome/free-solid-svg-icons";
import { Link } from "react-scroll";
import '../css/Navbar.css';


const Navbar = () => {

  return (
      <nav className={"portfolio-navbar"}>
        <ul className={"nav-list"}>
          <li>
            <Link activeClass="active-section" to="test1" spy={true} smooth={true} duration={500}>
              <div className="nav-circle">
                <div className="nav-circle-spin-one" style={{backgroundImage: "conic-gradient(transparent,transparent,transparent, red)"}}></div>
                <div className="nav-circle-spin-two" style={{backgroundImage: "conic-gradient(transparent,transparent,transparent, red)"}}></div>
                <span></span>
                <FontAwesomeIcon className={"nav-icon"} icon={faHouse} style={{color: "red"}}/> 
              </div>
              <h6 style={{color: "red"}}>Home</h6> 
            </Link>
          </li>
          <li> 
            <Link activeClass="active-section" to="test2" spy={true} smooth={true} duration={500}>
              <div className="nav-circle">
                <div className="nav-circle-spin-one" style={{backgroundImage: "conic-gradient(transparent,transparent,transparent, blue)"}}></div>
                <div className="nav-circle-spin-two" style={{backgroundImage: "conic-gradient(transparent,transparent,transparent, blue)"}}></div>
                <span></span>
                <FontAwesomeIcon className={"nav-icon"} icon={faAddressCard} style={{color: "blue"}}/> 
              </div>
              <h6 style={{color: "blue"}}>About Me</h6> 
            </Link>
          </li>
          <li>
            <Link activeClass="active-section" to="test3" spy={true} smooth={true} duration={500}>
              <div className="nav-circle">
                <div className="nav-circle-spin-one" style={{backgroundImage: "conic-gradient(transparent,transparent,transparent, green)"}}></div>
                <div className="nav-circle-spin-two" style={{backgroundImage: "conic-gradient(transparent,transparent,transparent, green)"}}></div>
                <span></span>
                <FontAwesomeIcon className={"nav-icon"} icon={faDiagramProject} style={{color: "green"}}/> 
              </div>
              <h6 style={{color: "green"}}>Projects</h6> 
            </Link>
          </li>
          <li>
            <Link activeClass="active-section" to="test4" spy={true} smooth={true} duration={500}>
              <div className="nav-circle">
                <div className="nav-circle-spin-one" style={{backgroundImage: "conic-gradient(transparent,transparent,transparent, yellow)"}}></div>
                <div className="nav-circle-spin-two" style={{backgroundImage: "conic-gradient(transparent,transparent,transparent, yellow)"}}></div>
                <span></span>
                <FontAwesomeIcon className={"nav-icon"} icon={faAddressBook} style={{color: "yellow"}}/> 
              </div>
              <h6 style={{color: "yellow"}}>Contact</h6> 
            </Link>
          </li>
        </ul>
      </nav>
  )
}

export default Navbar;
