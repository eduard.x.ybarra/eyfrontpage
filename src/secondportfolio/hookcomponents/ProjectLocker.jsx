import React from 'react';
import { useState, useEffect } from 'react';
import openeye from '../images/openeye.png';
import closedeye from '../images/closeeye.png';
import '../css/ProjectLocker.css';

const ProjectLocker = ({ projectNumber, selectedProject, activeSelectedProject }) => {
  const[clicked, setClicked] = useState(false);

  useEffect(() => {
    if(projectNumber === selectedProject) {
      setClicked(true);
    }
  }, [selectedProject])

  const handleClick = () => {
    setClicked(true);
    activeSelectedProject(projectNumber);
  }

  return (
    <div className={"project-lock-wrapper"} >
      <div className={"project-lock"}>
        <div className={`project-lock-circle ${projectNumber === selectedProject ? ("selected-circle-animation") : ("circle-animation")}`} onClick={() => handleClick()} >
          {clicked === false ? (
            <img src={closedeye} alt=""></img>
          ) : (
            <img src={openeye} alt=""></img>
          )}
        </div>
      </div>
      <div className={`project-lock-background project-lock-unselected`}/>
    </div>
  )
}

export default ProjectLocker;