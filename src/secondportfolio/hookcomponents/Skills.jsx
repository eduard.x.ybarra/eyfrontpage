import React from 'react';
import Tilt from 'react-parallax-tilt';

import '../css/Skills.css';

import { faBook } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

const Skills = () => {
  return (
    <Tilt transitionSpeed={5000} tiltEnable={false} glareEnable={true} glareMaxOpacity={0.3} glareColor="white" glareBorderRadius="20px" glarePosition='all' className={"skills-section"}>
      <div className={"skills-title"}>
        <div className={"skills-icon-box"}>
          <FontAwesomeIcon icon={faBook} size="2x"/>
        </div>
        <p>Skills</p>
      </div>
      <div style={{position: "relative"}}>
        <div className={"skills-parts"}>
          <div>
            <strong>
              Programming Languages
            </strong>
            <p className={"skills-name"}>Javascript (ES6+), Java, Python, HTML5, CSS</p>
          </div>
          <div>
            <strong>
              Framework
            </strong>
            <p className={"skills-name"}>ReactJS, NodeJS</p>
          </div>
          <div>
            <strong>
              Tools
            </strong>
            <p className={"skills-name"}>jQuery, Git</p>
          </div>
          <div>
            <strong>
              Database
            </strong>
            <p className={"skills-name"}>MySQL, MongoDB</p>
          </div>
          <div>
            <strong>
              Languages
            </strong>
            <p className={"skills-name"}>Swedish, English, Russian</p>
          </div>
          <div>
            <strong>
              Other
            </strong>
            <p className={"skills-name"}>Android Studios, Redux, Tradingview, Photoshop, FL</p>
          </div>
        </div>
      </div>
    </Tilt>
  )
}

export default Skills;