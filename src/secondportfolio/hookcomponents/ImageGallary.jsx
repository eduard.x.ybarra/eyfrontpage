import React from 'react';
import '../css/ImageGallary.css';

const ImageGallary = ({ photos }) => {

	return (
		<div className={"image-gallary"} style={{justifyContent: photos.length < 3 ? ("center") : ("")}}>
    {photos.map((image, index) => (
        <div className={"gallary-box"} style={{overflow: photos.length < 3 ? ("visible") : ("hidden"), width: photos.length < 3 ? ("auto") : ("")}}>
          <img src={require('../images/' + image)} alt={image}/>
        </div>
    ))}
		</div>
	)
}

export default ImageGallary;