import React, { useState, useEffect } from 'react';
import Navbar from './Navbar.jsx'; 
import Home from './Home.jsx'; 
import About from './About.jsx';
import Projects from './Projects.jsx';
import Contact from './Contact.jsx';
import SectionSeperator from './SectionSeperator.jsx';
import aboutDataJson from '../data/aboutData.json';
import projectsDataJson from '../data/projectsData.json';
import '../css/SecondPortfolio.css';

const SecondPortfolio = () => {
  const [aboutData, setAboutData] = useState([]);
  const [projectsData, setProjectsData] = useState([]);

  useEffect(() => {
    const loadData = () => {
      const loadedAboutData = [];
      const loadedProjectsData = [];

      for(let data of aboutDataJson) {
        loadedAboutData.push(
          data
        );
      }

      for(let data of projectsDataJson) {
        loadedProjectsData.push(
          data
        );
      }

      setAboutData(loadedAboutData);
      setProjectsData(loadedProjectsData);
    }
    loadData();
  }, []);

  if(aboutData[0] === undefined) {
    return (
      <div>Loader</div>
    )
  }

  return (
    <div className="second-portfolio">
      <Navbar />
      <div id={"test1"}>
        <Home />
      </div>
      <div id={"test2"}>
        <SectionSeperator topColor={"red"} bottomColor={"blue"}/>
        <About aboutData={aboutData} />
      </div>
      <div id={"test3"}>
        <SectionSeperator topColor={"blue"} bottomColor={"green"}/>
        <Projects projectsData={projectsData} />
      </div>
      <div id={"test4"}>
        <SectionSeperator topColor={"green"} bottomColor={"yellow"} />
        <Contact />
      </div>
    </div>
  );
}

export default SecondPortfolio;